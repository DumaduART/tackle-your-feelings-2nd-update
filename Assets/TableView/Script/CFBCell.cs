using UnityEngine;
using System.Collections;
using FBKit;

public class CFBCell : CCell 
{
	JSONObject _jsonObject;
	string mImageURL = "";
	// Use this for initialization
	void Start () 
	{
		if(_Textfield1 != null)
			_Textfield1.GetComponent<Renderer>().material.color = _TextField1Color;
		
		if(_Textfield2 != null)
			_Textfield2.GetComponent<Renderer>().material.color = _TextField2Color;
		
		if(_Textfield3 != null)
			_Textfield3.GetComponent<Renderer>().material.color = _TextField3Color;
		
		if(_Textfield4 != null)
			_Textfield4.GetComponent<Renderer>().material.color = _TextField4Color;
				
		if(_ImageField2 != null && _ImageFieldTexture2 != null)
			_ImageField2.GetComponent<Renderer>().material.mainTexture = _ImageFieldTexture2;
		
		refreshData();
	}
	
	public override void initializeWithJSONString(string pJSONString,GameObject cam)
	{
		//Debug.Log(pJSONString);
		
		_jsonObject = new JSONObject(pJSONString);
		mImageURL = _jsonObject.GetField("Picture").str;
		refreshData();
		if(mImageURL != "")
			StartCoroutine(LoadImageFromURL(mImageURL));
	}
	
	IEnumerator LoadImageFromURL(string url)
	{
		//Debug.Log("initializeWithJSONString=="+url);
		WWW www = new WWW(url);
		yield return www;
		if(www.error == null)			
		{
			//Debug.Log("initializeWithJSONString22");
			Texture2D tex = new Texture2D(256, 256, TextureFormat.RGB24, false);
			//Debug.Log("Got texture");
			 www.LoadImageIntoTexture(tex);
			//Texture2D tex=www.texture;
			//tex.Compress(true);
			tex.wrapMode = TextureWrapMode.Clamp;
			tex.anisoLevel = 9;
			tex.filterMode = FilterMode.Bilinear;
			_ImageField1.GetComponent<Renderer>().material.mainTexture = tex;
		}
		else
		{
			//Debug.Log("initializeWithJSONString33"+www.error);
		}
	}
	
	public override void refreshData()
	{		
		if(_jsonObject != null)
		{
			if(_jsonObject.HasField("Name"))
				if(_Textfield1!= null)
					_Textfield1.text = _jsonObject.GetField("Name").str;
			
			if(_jsonObject.HasField("Score"))
				if(_Textfield2!= null)
					_Textfield2.text = "Score:"+_jsonObject.GetField("Score").str;
			
			if(_jsonObject.HasField("UserId"))
				_CellUserId= _jsonObject.GetField("UserId").str;
			
			if(_jsonObject.HasField("Rank"))
				if(_Textfield3!= null)
					_Textfield3.text = " "+_jsonObject.GetField("Rank").str;	
		}
	}
	
	void loadPhotoFromNet()
	{
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
}

﻿using UnityEngine;
using System.Collections;
using MonetizationHelper;

namespace MonetizationHelper
{
public class ScoreHandler
{
	static ScoreHandlerImplementation mInstance = null;
	
	public static ScoreHandlerImplementation Instance
	{
		get
		{
			if(mInstance == null)
			{
				GameObject tObject = new GameObject();
				tObject.name = "ScoreHandler Instance";
				mInstance = tObject.AddComponent<ScoreHandlerImplementation>();
			}
			return mInstance;
		}
	}
	
	public static void InstanceDestroyed()
	{
		mInstance = null;
	}
}
}
﻿using UnityEngine;
using System.Collections;
using FBKit;
using MonetizationHelper;

namespace MonetizationHelper
{
public class CScoreCell : MonetizationHelper.CCell
{
	public Texture _TrophyTexture;
	public Color _ScoreTextColor = Color.white;
	public Color _DateTextColor = Color.white;
	public Color _RankTextColor = Color.white;
	JSONObject mJSONObject;
	
	// Use this for initialization
	void Start () 
	{
		if(_Textfield1 != null)
			_Textfield1.GetComponent<Renderer>().material.color = _ScoreTextColor;
		
		if(_Textfield2 != null)
			_Textfield2.GetComponent<Renderer>().material.color = _DateTextColor;
		
		if(_Textfield3 != null)
			_Textfield3.GetComponent<Renderer>().material.color = _RankTextColor;
	}
	
	public override void initializeWithJSONString(string pJSONString)
	{
		_UserData = pJSONString;
		refreshData();
	}
	
	public override void refreshData()
	{
		mJSONObject = new JSONObject(_UserData);
		mJSONObject.type = JSONObject.Type.OBJECT;
		
		//REFRESH UR DATA HERE
		string RankString = ""+(int)mJSONObject.GetField("Rank").n;
		string DateString = "DATE : "+mJSONObject.GetField("Date").str;
		string ScoreString = "SCORE : "+(int)mJSONObject.GetField("Score").n;
		
		if(_Textfield1 != null)
			_Textfield1.text = RankString;
		
		if(_Textfield2 != null)
			_Textfield2.text = ScoreString;
		
		if(_Textfield3 != null)
			_Textfield3.text = DateString;
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
}
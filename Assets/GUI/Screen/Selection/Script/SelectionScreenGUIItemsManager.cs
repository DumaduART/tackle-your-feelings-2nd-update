using UnityEngine;
using System.Collections;

public class SelectionScreenGUIItemsManager : GUIItemsManager
{
	public GameObject MainBackGround;	

	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	private InputManager _inputManager;
	
	bool tutorial_level = false;
	
	// Use this for initialization
	void Start ()
	{
		base.Init();	
		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		_inputManager = GameObject.Find("GameManager").GetComponent("InputManager") as InputManager ;
		subscreenitemmanager = GameObject.Find("ShopSubScreenManager").GetComponent<ScreenManager>();
	}
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
			//Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "BackBtn")
			{
				
				_screenManager.LoadScreen(_screenManager.prevPageBeforeSelection);
				
			}
			
			if(item.name == "PlayBtn")
			{
				_screenManager.LoadScreen("Empty");

				if(_gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
				{
					_gameManager.LoadChallenge(_gameManager.GetIndex());
					PoleMovementRotation polemovement = _gameManager.GetChallenge().GetComponent<PoleMovementRotation>();
					if(polemovement!=null)
					{
						polemovement.Enable();
					}
				}
				EventManager.GameStartTrigger();
//				
				SoundManager.instance.SoundStatefunction(SoundManager.soundstateInGame.gameplay);
			}
			
			if(item.name == "bSelectEnvironment")
			{
				_screenManager.userdata = "SelectionScreen";
				_screenManager.LoadScreen("ShopScreen");
				//subscreenitemmanager.LoadScreen("EnvionmentSelectionScreen");
				subscreenitemmanager.defaultScreen = "EnvionmentSelectionScreen";
			}
			
			if(item.name == "bSelectBall")
			{
				_screenManager.userdata = "SelectionScreen";
				_screenManager.LoadScreen("ShopScreen");
				//subscreenitemmanager.LoadScreen("BallSelectionScreen");
				subscreenitemmanager.defaultScreen = "BallSelectionScreen";
			}
			
			if(item.name == "bSelecAddUps")
			{
				_screenManager.userdata = "SelectionScreen";
				_screenManager.LoadScreen("ShopScreen");
				//subscreenitemmanager.LoadScreen("AddupsSelectionScreen");
				subscreenitemmanager.defaultScreen = "AddupsSelectionScreen";
			}
			
			
			
			
		}
		
		
		
	}	
}
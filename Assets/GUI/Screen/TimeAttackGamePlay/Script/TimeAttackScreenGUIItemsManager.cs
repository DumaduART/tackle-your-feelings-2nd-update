using UnityEngine;
using System.Collections;

public enum TextType
{
	score,timer,multiplier
};
public class TimeAttackScreenGUIItemsManager : GUIItemsManager
{
	public GameObject MainBackGround;	
	public GUITexture LeftwindDir;
	public GUITexture rightWindDir;
	public GUITexture windBg;
	public GUITexture distanceBg;
	public GUIText windValue;
	public GUIText wind;
	public GUIText mph;
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	private InputManager _inputManager;
	 PopUpManager _popupmanager;
	bool tutorial_level = false;
	public static  TimeAttackScreenGUIItemsManager instance;
	public GuiTextManager _guitextmanager;
	public GameObject TaptoSkip;
	// Use this for initialization
	
	public GUIText _scoreText;
	public GUIText _ScoreNameText;
	public GUITexture _ScoreValueBg;
	
	public GUIText _multiplierText;
	public GUIText _multiplierNameText;
	public GUITexture _multiplierValueBg;
	public GUIText _multiplierValuesignText;
	
	public GUIText _timerText;
	
	Vector3 defaultScale;
	public GameObject _gBallSelectionScreenManager;
	
	ArrayList AnimationObjects = new ArrayList();
	
	TimeAttack timeAttack;
	
	static GameObject ballSelectionClone;
	
	Vector3 targetScale = new Vector3(0.2f , 0.15f , 0.0f);
	Vector3 defaltScale = new Vector3(0.15f , 0.1125f , 0.0f);
	
	bool Ischeck = true;
	public bool isActivate = false;
	int	index = 0;
	
	
	public bool _isAndroidBack = false;
	void Awake()
	{
		instance=this;

		ballSelectionClone = Instantiate (_gBallSelectionScreenManager)as GameObject;
//		if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.M_Sports_Mode)
//			Invoke ("HideBallSelect", 0.01f);
//		#if UNITY_ANDROID
//		_scoreText.GetComponent<MenuItemEntryAnim>()._EndPos.y = .80f;
//		_ScoreNameText.GetComponent<MenuItemEntryAnim>()._EndPos.y = .84f;
//		_ScoreValueBg.GetComponent<MenuItemEntryAnim>()._EndPos.y = .805f;
//		_multiplierText.GetComponent<MenuItemEntryAnim>()._EndPos.y = .80f;
//		_multiplierNameText.GetComponent<MenuItemEntryAnim>()._EndPos.y = .84f;
//		_multiplierValuesignText.GetComponent<MenuItemEntryAnim>()._EndPos.y = .80f;
//		_multiplierValueBg.GetComponent<MenuItemEntryAnim>()._EndPos.y = .805f;
//		_scoreText.GetComponent<MenuItemExitAnim>()._EndPos.y = .80f;
//		_ScoreNameText.GetComponent<MenuItemExitAnim>()._EndPos.y = .84f;
//		_ScoreValueBg.GetComponent<MenuItemExitAnim>()._EndPos.y = .805f;
//		_multiplierText.GetComponent<MenuItemExitAnim>()._EndPos.y = .80f;
//		_multiplierNameText.GetComponent<MenuItemExitAnim>()._EndPos.y = .84f;
//		_multiplierValuesignText.GetComponent<MenuItemExitAnim>()._EndPos.y = .80f;
//		_multiplierValueBg.GetComponent<MenuItemExitAnim>()._EndPos.y = .805f;
//		#endif
	}
	
	void Start ()
	{
		
		base.Init();	
		
		_gameManager = GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		_inputManager = GameObject.Find("GameManager").GetComponent("InputManager") as InputManager ;
		_popupmanager = GameObject.Find("GameManager").GetComponent<PopUpManager>();
		timeAttack = TimeAttack.GetInstance();
		ChangeWindImage(timeAttack.GetWindType());
		EventManager.GameOver += GameOver ;
//		setAsTabButton("Pause");
//		setTabButtonActive(0,true);
		//defaltScale = windBg.transform.localScale;
		//targetScale = 1.3f * defaltScale;		
	}

	void HideBallSelect()
	{
		ballSelectionClone.GetComponent<ScreenManager>().closeScreenManager();
	}

	void OnDestroy()
	{
		EventManager.GameOver -= GameOver ;
	}
//	public void ActivatePauseButton()
//	{
//		setTabButtonActive(0,false);
//	}
	
	public static GameObject GetBallSelectionClone()
	{
		return(ballSelectionClone);
	}
	
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
			
	}	
	
	public override void OnEntryAnimationCompleted()
	{
		defaltScale = distanceBg.transform.localScale;
		_isAndroidBack = true;
	}
	
	public override void OnSelectedEvent(GUIItem item)
	{

//		  throw new UnassignedReferenceException();

		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{

			//Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "Pause")
			{
					if(_inputManager.GetRefOfBall() == null)
					return;
					if(_inputManager.GetRefOfBall().GetComponent<CollisionDetection>().ballStatus == CollisionDetection.BallStatus.nothitted && TimeAttack.GetTimeLeft()>1)
					{
					    //Debug.Log("Insideloop");
						StopAllActions();
                    if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode)
					{
						_screenManager.LoadScreen("PauseMspo");
					}
					else
					{
						_screenManager.LoadScreen("Pause");
					}
						EventManager.GamePauseTrigger();   
						_popupmanager.HidePopUp();
					    if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode)
						ballSelectionClone.GetComponent<ScreenManager>().closeScreenManager();
				    }

			}
			
			if(item.name == "P1Btn")
			{
				GameObject ball = _inputManager.GetRefOfBall();
				PowerUpsManager _powerManager = PowerUpsManager.instance;
				if(ball.GetComponent<Ball>()._ballProperty._powerType!=_powerManager._SelectedPowerups[0]._poweruptype)
				{
					ball.GetComponent<Ball>()._ballProperty._powerType = _powerManager._SelectedPowerups[0]._poweruptype;
					if( _powerManager._SelectedPowerups[0]._count>0)
					 _powerManager._SelectedPowerups[0]._count  -= 1;
					_guitextmanager.UpdateText();
				}
			}
			
			if(item.name == "P2Btn")
			{
				GameObject ball = _inputManager.GetRefOfBall();
				PowerUpsManager _powerManager = PowerUpsManager.instance;
				if(ball.GetComponent<Ball>()._ballProperty._powerType!=_powerManager._SelectedPowerups[1]._poweruptype)
				{
					ball.GetComponent<Ball>()._ballProperty._powerType = _powerManager._SelectedPowerups[1]._poweruptype;
					if( _powerManager._SelectedPowerups[1]._count>0)
					 _powerManager._SelectedPowerups[1]._count  -= 1;
					_guitextmanager.UpdateText();
				}
			}
			
			if(item.name == "P3Btn")
			{
				GameObject ball = _inputManager.GetRefOfBall();
				PowerUpsManager _powerManager = PowerUpsManager.instance;
				if(ball.GetComponent<Ball>()._ballProperty._powerType!=_powerManager._SelectedPowerups[2]._poweruptype)
				{
					ball.GetComponent<Ball>()._ballProperty._powerType = _powerManager._SelectedPowerups[2]._poweruptype;
					if( _powerManager._SelectedPowerups[2]._count>0)
					 _powerManager._SelectedPowerups[2]._count  -= 1;
					_guitextmanager.UpdateText();
				}
			}
			
		}
		
		
		
	
	}	
	public void ChangeWindImage(TimeAttack.WindType  windType)
	{
		
		TimeAttack timeAttack = TimeAttack.instance;
		if( Mathf.Abs(timeAttack.GetWind())>0)
		{
			windBg.gameObject.SetActive(true);
			windValue.gameObject.SetActive(true);
			wind.gameObject.SetActive(true);
			mph.gameObject.SetActive(true);
			switch(windType)
			{
				case TimeAttack.WindType.left:
					LeftwindDir.gameObject.SetActive(true);
					rightWindDir.gameObject.SetActive(false);
					break;
				case TimeAttack.WindType.right:
					rightWindDir.gameObject.SetActive(true);
					LeftwindDir.gameObject.SetActive(false);
					break;
			}
		}
		else
		{
			rightWindDir.gameObject.SetActive(false);
			LeftwindDir.gameObject.SetActive(false);
//			windBg.gameObject.SetActive(false);
//			windValue.gameObject.SetActive(false);
//			wind.gameObject.SetActive(false);
//			mph.gameObject.SetActive(false);
		}
		
			
	}
	
	public void Activate(bool isActivate)
	{
		TaptoSkip.SetActive(isActivate);
	}
	GUIText availabaleText = null;
	GameObject mAnimationObject = null;
	
	public void StopActions(GameObject pObj)
	{
		if(pObj != null)
		{
			Destroy(pObj);
			AnimationObjects.Remove(pObj);
		}
		
		if(availabaleText!=null)
		availabaleText.transform.localScale = defaultScale;
	}
	
	public void StopAllActions()
	{
		foreach(GameObject tObj in AnimationObjects)
			Destroy(tObj);
		
		AnimationObjects.Clear();
	}
	
	
	
	
	public void StartZoomInZoomOut(TextType textType)
	{
		
		switch(textType)
		{
			case TextType.score:
			{
				availabaleText = _scoreText;
				break;
			}
			
			case TextType.multiplier:
			{
				availabaleText = _multiplierText ;
				break;
			}
			
			case TextType.timer:
			{
				availabaleText = _timerText ;
				break;
			}
		}
		
		defaultScale = availabaleText.gameObject.transform.localScale;
		GameObject tAnimObject = new GameObject();
		tAnimObject.name = "Trial animation";
		Vector3 scale = availabaleText.gameObject.transform.localScale;
		
		CScaleTo tScale = tAnimObject.AddComponent<CScaleTo>();
		tScale.actionWith(availabaleText.gameObject,new Vector2(scale.x*1.2f,scale.y*1.2f),1.0f);
		
		CEaseElastic tease1 = tAnimObject.AddComponent<CEaseElastic>();
		tease1.actionWithAction(tScale,EaseType.EaseOut);
		
		CScaleTo tScale2 = tAnimObject.AddComponent<CScaleTo>();
		tScale2.actionWith(availabaleText.gameObject,new Vector2(scale.x,scale.y),1.0f);
		
		CEaseElastic tease2 = tAnimObject.AddComponent<CEaseElastic>();
		tease2.actionWithAction(tScale2,EaseType.EaseOut);
		
		CSequence seq1 = tAnimObject.AddComponent<CSequence>();
		seq1.actionWithActions(tease1,tease2);
		
		CCallFuncWithObj call = tAnimObject.AddComponent<CCallFuncWithObj>();
		call.actionWithCallBack(StopActions,tAnimObject);
		
		CSequence seq = tAnimObject.AddComponent<CSequence>();
		seq.actionWithActions(seq1,call);
		AnimationObjects.Add(tAnimObject);

		seq.runAction();
	}
	
	public void ScalUp()
	{
		
		windBg.GetComponent<GUITexture>().color = Color.red;
		distanceBg.GetComponent<GUITexture>().color = Color.red;
		Vector3 tempScale;
		tempScale = defaltScale;
		while(tempScale!=targetScale)
		{
			tempScale = Vector3.MoveTowards(tempScale,targetScale,.0001f*Time.time);
			windBg.transform.localScale = tempScale;
			distanceBg.transform.localScale = tempScale;
			if(tempScale==targetScale)
			{
				print ("ScaleDown");
				ScalDown();
			}
			
			else
			{
				print ("scaleUp");
			}
		}
		
//		while(tempScale!= defaltScale)
//		{
//			tempScale = Vector3.MoveTowards(tempScale,defaltScale,.0002f);
//			windBg.transform.localScale = tempScale;
//			distanceBg.transform.localScale = tempScale;
//		}
	}
	
	public void ScalDown()
	{
		windBg.GetComponent<GUITexture>().color = Color.green;
		distanceBg.GetComponent<GUITexture>().color = Color.green;
		Vector3 tempScale;
		tempScale = targetScale;
		while(tempScale!= defaltScale)
		{
			tempScale = Vector3.MoveTowards(tempScale,defaltScale,.0001f);
			windBg.transform.localScale = tempScale;
			distanceBg.transform.localScale = tempScale;
		}
	}
	
	
	void Update()
	{
		if(isActivate)
		{
			if(Ischeck)
			{
				
				distanceBg.transform.localScale = Vector3.MoveTowards(distanceBg.transform.localScale , targetScale , 0.4f * Time.deltaTime);
				windBg.transform.localScale = Vector3.MoveTowards(distanceBg.transform.localScale , targetScale , 0.4f * Time.deltaTime);		
				if(distanceBg.transform.localScale.Equals(targetScale))
				{
					Ischeck = false;
					index++;
				}
				
			}
			else
			{
				distanceBg.transform.localScale = Vector3.MoveTowards(distanceBg.transform.localScale , defaltScale , 0.3f * Time.deltaTime);
				windBg.transform.localScale = Vector3.MoveTowards(distanceBg.transform.localScale , defaltScale , 0.3f * Time.deltaTime);
				if(distanceBg.transform.localScale.Equals(defaltScale))
				{
					Ischeck = true;
					if(index == 2)
					{
						isActivate = false;
						index = 0;
					}
				}
			}
		}
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(_isAndroidBack)
			{
				if(_inputManager.GetRefOfBall() == null)
					return;
				if(_inputManager.GetRefOfBall().GetComponent<CollisionDetection>().ballStatus == CollisionDetection.BallStatus.nothitted && TimeAttack.GetTimeLeft()>1)
				{
					StopAllActions();
					_screenManager.LoadScreen("Pause");
					EventManager.GamePauseTrigger();
					_popupmanager.HidePopUp();
					if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode)
					   ballSelectionClone.GetComponent<ScreenManager>().closeScreenManager();
					 _isAndroidBack = false;
			    }
				
			}
		}
	}
	
	
	
	public override void OnExitAnimationCompleted()
	{
		StopAllActions();
	}
	
	
	private void GameOver()
	{
		StopAllActions();
	}
	
}
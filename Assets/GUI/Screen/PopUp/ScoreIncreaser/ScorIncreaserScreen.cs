using UnityEngine;
using System.Collections;

public class ScorIncreaserScreen : GUIItemsManager
{
	public enum IncreaserType
	{
		score,multiplier,time
	};
	public IncreaserType increaserType;
	public GameObject MainBackGround;	
	public GameObject ring;
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	GameManager _gameManager;
	PopUpManager _popUpManager;
	bool tutorial_level = false;
	public GUIText targetText;
	public GameObject targetGameObject;
	
	// Use this for initialization
	void Start ()
	{
		
		_gameManager = GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;		
		_popUpManager = GameObject.Find("GameManager").GetComponent<PopUpManager>();
		_popUpManager.AddToList(this.gameObject);
		base.Init();	
		switch(increaserType)
		{
			
			case IncreaserType.score:
			{
				targetGameObject =  GameObject.Find("ScoreValue") as GameObject;
				targetText.text = "+" + ScoreManager.instance.GetCurrentShotScore().ToString();
				break;
			}
			
			case IncreaserType.multiplier:
			{
				targetGameObject = GameObject.Find("MultiPlierValue") as GameObject;
				targetText.text = "+" + "1";
				break;
			}
			
			case IncreaserType.time:
			{
				targetGameObject = GameObject.Find("Timer Value") as GameObject;
				if(TimeAttack.instance.GetFlashingZoneCondition())
				{
					targetText.text = "+" + "2";
				}
				
				else
				{
					targetText.text = "+" + "1";
				}
					
				break;
			}
			
		}
		
		
	}
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			
		
		}
		
		
		
		
	}	
	
	public override void OnEntryAnimationCompleted()
	{
		this.onExitCompleteCallBack = managerExitCallBack;
		this.State = eGUI_ITEMS_MANAGER_STATE.Exiting;
		
	}
	
	void managerExitCallBack()
	{
		
		if(GuiTextManager.instance!=null)
		GuiTextManager.instance.UpdateText();
		
		switch(increaserType)
		{
			case IncreaserType.time:
			{
				if(TimeAttack.instance.GetFlashingZoneCondition())
				{
					TimeAttack.instance.SetTime(2);
				}
				
				else
				{
					TimeAttack.instance.SetTime(1);
				}
				break;
			}
		}
		
		switch(_gameManager.GetGameMode())
		{
			case GameManager.GameMode.ClockShowDown:
			{
				if(TimeAttackScreenGUIItemsManager.instance!=null)
				{
					switch(increaserType)
						{
							
							case IncreaserType.score:
							{
								TimeAttackScreenGUIItemsManager.instance.StartZoomInZoomOut(TextType.score);
								break;
							}
							
							case IncreaserType.multiplier:
							{
								TimeAttackScreenGUIItemsManager.instance.StartZoomInZoomOut(TextType.multiplier);
								break;
							}
							
							case IncreaserType.time:
							{
								TimeAttackScreenGUIItemsManager.instance.StartZoomInZoomOut(TextType.timer);
								break;
							}
						}
				}
				break;
			}
			
			case GameManager.GameMode.SuddenDeath:
			{	
				if(SuddenDeathGUIItemsManager.instance!=null)
				{
					switch(increaserType)
						{
							
							
							case IncreaserType.score:
							{
								SuddenDeathGUIItemsManager.instance.StartZoomInZoomOut(TextType.score);
								break;
							}
							
							case IncreaserType.multiplier:
							{
								SuddenDeathGUIItemsManager.instance.StartZoomInZoomOut(TextType.multiplier);
								break;
							}
							
							case IncreaserType.time:
							{
//								SuddenDeathGUIItemsManager.instance.StartZoomInZoomOut(TextType.timer);
								break;
							}
						}
				}
				break;
			}
		}
		_popUpManager.RemoveFromList(this.gameObject);
		Destroy(this.gameObject);
		
	}
	
}
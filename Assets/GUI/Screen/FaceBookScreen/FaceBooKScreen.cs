using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FaceBooKScreen : GUIItemsManager
{
	GameManager _gameManager;
	public GameObject MainBackGround ,_LoadingText;	
	public GameObject ring;
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	
	
	bool tutorial_level = false;
	
	public GUIText _ModeText;
	
	public GUIText[] _nameText;
	public GUIText[] _scoreText;
	string [] mStrarray = new string[5];
	bool isplay,mbObjIsThere = true;
	
	bool _isAndroidBack = false;
	
	public GUITexture loginbtn;
	void Awake()
	{
		_gameManager = GameManager.GetInstance();		
	}
	
	// Use this for initialization
	void Start ()
	{
		base.Init();	
		
		//Instantiate(ring);
		_gameManager.mainScreenManager.DisableInput();
		if(_gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown)
		{
			_ModeText.text = "CLOCK SHOWDOWN";
		}
		else if(_gameManager.GetGameMode() == GameManager.GameMode.SuddenDeath)
		{
			_ModeText.text = "SUDDEN DEATH";
		}
		
//		if(OnlineSocialHandler.Instance.GetIsLoggedIn())
//		{
//			loginbtn.gameObject.SetActive(false);
//			MainScreenGUIItemsManager.instance. _loginButton.SetActive(false);
//			MainScreenGUIItemsManager.instance._logoutButton.SetActive(true);
//		}
		
		
		_LoadingText.GetComponent<GUIText>().text = "Loading friends data...";
		MainScreenGUIItemsManager.instance._isAndroidBack = false;
	}
	
	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			if(_isAndroidBack)
			{
				mbObjIsThere = false;
				_gameManager.FaceBookSubScrren.LoadScreen("Empty");
				MainScreenGUIItemsManager.instance.TableviewCamEnable(true);
				MainScreenGUIItemsManager.instance.SetData();
				_isAndroidBack = false;
				MainScreenGUIItemsManager.instance._isAndroidBack = true;
			}
		}
	}
	
	
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			
			if(item.name == "CloseBtn")
			{
				 print ("Closed");
				mbObjIsThere = false;
				_gameManager.FaceBookSubScrren.LoadScreen("Empty");
				MainScreenGUIItemsManager.instance.TableviewCamEnable(true);
				MainScreenGUIItemsManager.instance.SetData();
				MainScreenGUIItemsManager.instance._isAndroidBack = true;
			}
			
			if(item.name=="FaceBook_Login")
			{
				print ("Clicked");
				OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.LOGIN,"",CallBack);
				
			}
			
			if(item.name == "PlayBtn" )
			{
				mbObjIsThere = false;
				_gameManager.FaceBookSubScrren.LoadScreen("Empty");
				isplay = true;
			}
		
		}
		
		
		
		
	}	
	
	public override void OnExitAnimationCompleted()
	{
		if(isplay)
		{
			if(PlayerPrefs.GetString("tuetorial")=="tuetorialoff")
			   _gameManager.mainScreenManager.LoadScreen("Empty");
			else
			  _gameManager.mainScreenManager.LoadScreen("Loading");

		}
		else
		{
			_gameManager.mainScreenManager.EnableInput();
		}
	}
	public override void OnEntryAnimationCompleted ()
	{
		if(_gameManager.GetGameMode() == GameManager.GameMode.SuddenDeath )
			OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.GET_TOP_N_RANKING_LIST,"5",CallBack,"SuddenDeathScore");
		else if(_gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown )
			OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.GET_TOP_N_RANKING_LIST,"5",CallBack,"TimeScore");
		
		_isAndroidBack = true;
	}
	void CallBack(string strResult ,eONLINE_REQ_TYPE eRequestType,IList<UserData> data = null)
	{
		
		if(!mbObjIsThere)
		{
			return;
		}
		if(eRequestType == eONLINE_REQ_TYPE.LOGIN)
		{		
			
			if(_gameManager.GetGameMode() == GameManager.GameMode.SuddenDeath )
					OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.GET_TOP_N_RANKING_LIST,"5",CallBack,"SuddenDeathScore");
			else if(_gameManager.GetGameMode() == GameManager.GameMode.ClockShowDown )
				OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.GET_TOP_N_RANKING_LIST,"5",CallBack,"TimeScore");
			_LoadingText.GetComponent<GUIText>().text = "Loading friends data...";
			loginbtn.gameObject.SetActive(false);
			MainScreenGUIItemsManager.instance._loginButton.SetActive(false);
			MainScreenGUIItemsManager.instance._logoutButton.SetActive(true);
			//Change Login/Logout Image
		}
		
		else if(eRequestType == eONLINE_REQ_TYPE.NO_INTERNET_CONNECTION)
		{
			// connect to net first popup
		}
		//_gameManager.FaceBookSubScrren.EnableInput();
		if(eRequestType == eONLINE_REQ_TYPE.GET_TOP_RANKING_LIST || eRequestType == eONLINE_REQ_TYPE.GET_TOP_N_RANKING_LIST)
		{
			
			for(int i=0; i<5;i++)
			{
				if(i<data.Count)
				{
					string strVal = data[i]._UserRank.ToString()+" "+data[i]._UserName;
					strVal =strVal+","+data[i]._UserScore.ToString();
					mStrarray[i] = strVal;
				}
				else
				{
					mStrarray[i] = " , " ;
				}
				
			}
			
			SetLeaderboard();
			_LoadingText.GetComponent<GUIText>().text = "";
		}
		if(eRequestType == eONLINE_REQ_TYPE.NO_INTERNET_CONNECTION)
		{
			_LoadingText.GetComponent<GUIText>().text = "Connect to Facebook...";
		}

	}
	void SetLeaderboard()
	{
		for(int i=0; i<mStrarray.Length;i++)
		{
			string[] str =  mStrarray[i].Split(',');
			
			List<string> name = new List<string>();
			string t = str[0] + " ";
			char[] tempArray = t.ToCharArray();
			int checkpoint = 0;
			int j = 0;
			for(j = 0;j<tempArray.Length;j++)
			{
				if(tempArray[j] == ' ')
				{
					string n = t.Substring(checkpoint,(j-checkpoint));
					name.Add(n);
					checkpoint = j;
				}
			}
			t = "";
			for(j = 0 ;j<name.Count ; j++)
			{
				if(t.Length + name[j].Length > 15)
					break;
				t = t + name[j] + " ";
			}
			
			_nameText[i].text = t;
			_scoreText[i].text = str[1];
		}
	}
	
}
﻿using UnityEngine;
using System.Collections;

public class PositionSetting : MonoBehaviour {

	// Use this for initialization
	public GUIText[] _objectiveText;
	
	public GUIText[] _goldDecriptionValueText;
	public GUIText[] _bronzeDescriptionValueText;
	public GUIText[] _silverDescriptionValueText;
	
	public GUITexture[] _goldTrophy;
	public GUITexture[] _silverTrophy;
	public GUITexture[] _bronzeTrophy;
	public GUITexture[] _isTrophy;
	
	public Vector3 _goldDecriptionValueTextPos;
	public Vector3 _bronzeDescriptionValueTextPos;
	public Vector3 _silverDescriptionValueTextPos;
	public Vector3 _goldTrophyPos;
	public Vector3 _silverTrophyPos;
	public Vector3 _bronzeTrophyPos;
	public Vector3 _objectiveTextPos;
	
	public Font _descriptionFont;
	public Material _descriptionFontMaterial;
	public string[] _objectiveDescription;
	
	public Texture2D _goldTrophyImage;
	public Texture2D _silverTrophyImage;
	public Texture2D _bronzeTrophyImage;
	
	void Start () {
		SetPosition();
		ChkTrophy();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void SetPosition()
	{
		for(int i = 0 ; i< _goldDecriptionValueText.Length;i++)
		{
			_goldDecriptionValueText[i].transform.localPosition = _goldDecriptionValueTextPos;
			_goldDecriptionValueText[i].GetComponent<GUIText>().font = _descriptionFont;
			_goldDecriptionValueText[i].GetComponent<GUIText>().material = _descriptionFontMaterial;
		}
		
		for(int i = 0 ; i< _bronzeDescriptionValueText.Length;i++)
		{
			_bronzeDescriptionValueText[i].transform.localPosition = _bronzeDescriptionValueTextPos;
			_bronzeDescriptionValueText[i].GetComponent<GUIText>().font = _descriptionFont;
			_bronzeDescriptionValueText[i].GetComponent<GUIText>().material = _descriptionFontMaterial;
			
		}
		
		for(int i = 0 ; i< _silverDescriptionValueText.Length;i++)
		{
			_silverDescriptionValueText[i].transform.localPosition = _silverDescriptionValueTextPos;
			_silverDescriptionValueText[i].GetComponent<GUIText>().font = _descriptionFont;
			_silverDescriptionValueText[i].GetComponent<GUIText>().material = _descriptionFontMaterial;
			
		}
		
		for(int i = 0 ; i< _goldTrophy.Length;i++)
		{
			_goldTrophy[i].transform.localPosition = _goldTrophyPos;
			
		}
		
		for(int i = 0 ; i< _silverTrophy.Length;i++)
		{
			_silverTrophy[i].transform.localPosition = _silverTrophyPos;
			
		}
		
		for(int i = 0 ; i< _bronzeTrophy.Length;i++)
		{
			_bronzeTrophy[i].transform.localPosition = _bronzeTrophyPos;
			
		}
		
		for(int i = 0 ; i<_objectiveText.Length ; i++)
		{
			_objectiveText[i].transform.localPosition = _objectiveTextPos;
			_objectiveText[i].text = _objectiveDescription[i];
			_objectiveText[i].GetComponent<GUIText>().font = _descriptionFont;
			_objectiveText[i].GetComponent<GUIText>().material = _descriptionFontMaterial;
		}
		
		
	}
	
	public void ChkTrophy()
	{
		int temp =0;
		GameManager gameManager = GameManager.GetInstance();
		switch(gameManager.GetCategory())
		{
			case GameManager.Category.ThroughRings:
			{
				for(int i = 0;i<_isTrophy.Length;i++)
				{
					if(gameManager.UpdateIntDataInDevice("ThroughRings"+ i +"3",temp,true) == 1)
					{
					  _isTrophy[i].texture = _goldTrophyImage;
					}
				
					else if(gameManager.UpdateIntDataInDevice("ThroughRings"+ i +"2",temp,true) == 1)
					{
						_isTrophy[i].texture = _silverTrophyImage;
					}
				
					else if(gameManager.UpdateIntDataInDevice("ThroughRings"+ i +"1",temp,true) == 1)
					{
						_isTrophy[i].texture = _bronzeTrophyImage;
					}
				
					else
					_isTrophy[i].texture = null;
				}
				break;
			}
			
			case GameManager.Category.HitTheBar:
			{
				for(int i = 0;i<_isTrophy.Length;i++)
				{
					if(gameManager.UpdateIntDataInDevice("HitTheBar"+ i +"3",temp,true) == 1)
					{
					  _isTrophy[i].texture = _goldTrophyImage;
					}
				
					else if(gameManager.UpdateIntDataInDevice("HitTheBar"+ i +"2",temp,true) == 1)
					{
						_isTrophy[i].texture = _silverTrophyImage;
					}
				
					else if(gameManager.UpdateIntDataInDevice("HitTheBar"+ i +"1",temp,true) == 1)
					{
						_isTrophy[i].texture = _bronzeTrophyImage;
					}
				
					else
					_isTrophy[i].texture = null;
				}
				break;
			}
			
			case GameManager.Category.NoSwingStorm:
			{
				for(int i = 0;i<_isTrophy.Length;i++)
				{
					if(gameManager.UpdateIntDataInDevice("NoSwingStorm"+ i +"3",temp,true) == 1)
					{
					  _isTrophy[i].texture = _goldTrophyImage;
						gameManager.noOfThroughRingsGold++;
					}
				
					else if(gameManager.UpdateIntDataInDevice("NoSwingStorm"+ i +"2",temp,true) == 1)
					{
						_isTrophy[i].texture = _silverTrophyImage;
						gameManager.noOfHitTheBarGold++;
					}
				
					else if(gameManager.UpdateIntDataInDevice("NoSwingStorm"+ i +"1",temp,true) == 1)
					{
						_isTrophy[i].texture = _bronzeTrophyImage;
						gameManager.noOfNoswingGold++;
					}
				
					else
					_isTrophy[i].texture = null;
				}
				break;
			}
		}
	}
}


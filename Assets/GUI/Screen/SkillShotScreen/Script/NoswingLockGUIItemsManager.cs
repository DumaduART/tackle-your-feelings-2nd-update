using UnityEngine;
using System.Collections;

public class NoswingLockGUIItemsManager : GUIItemsManager
{
	public GameObject MainBackGround;	

	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	
	bool tutorial_level = false;
	
	// Use this for initialization
	void Start ()
	{
		base.Init();	
		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
	}
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
			Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "BackBtn")
			{
				_screenManager.LoadScreen("MainMenu");
				EventManager.GameHomeTrigger();
			}
			if(item.name == "LeftButton")
			{
				_screenManager.LoadScreen("HitTheBar");
				_gameManager.SetCategory(GameManager .Category.HitTheBar);
			}
			if(item.name == "RightButton")
			{
				print (item.name);
				if(_gameManager.GetCoins() >= 2000)
				{
					_screenManager.LoadScreen("ThroughRings");
					
				}
				
				else
					
					_screenManager.LoadScreen("ThroughRingsLocked");
				_gameManager.SetCategory(GameManager .Category.ThroughRings);
			}
			
			if(item.name == "PlayBtn")
			{
				_screenManager.LoadScreen("SkillShot");
				EventManager.GameStartTrigger();
			}
		}
		
		
		
	}	
}
using UnityEngine;
using System.Collections;

public class ChallengedescriptionGUIItemsManager : GUIItemsManager
{
	public GameObject MainBackGround;	

	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	
	bool tutorial_level = false;
	
	// Use this for initialization
	void Start ()
	{
		base.Init();	
		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
//		transform.GetComponent<ButtonList>().SetSelectedState("Challenge1");
	//	ButtonList.instance.SetSelectedState("Challenge1");
	}
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		//Debug.Log(item.name + "here");
		
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
			//Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "BackBtn")
			{
				print (item.name);
				GameObject.Find("ChallengeSubScreen(Clone)").GetComponent<ScreenManager>().closeScreenManager();
				SetActiveChallengeScreen();
				EventManager.GameHomeTrigger();
				switch(_gameManager.GetCategory())
				{
					case GameManager.Category.HitTheBar:
				{
					//GameObject.Find("HitTheBar(Clone)").GetComponent<HittheBarGUIItemsManager>().SetActive(true);
					ButtonList.instance.SetSelectedState(item.name,false);
					break;
				}
				
				case GameManager.Category.NoSwingStorm:
				{
					ButtonList.instance.SetSelectedState(item.name,false);
					break;
				}
				
				case GameManager.Category.ThroughRings:
				{
					ButtonList.instance.SetSelectedState(item.name,false);
					break;
				}
				}
			}
			
			if(item.name == "PlayBtn")
			{
				GameObject.Find("ChallengeSubScreen(Clone)").GetComponent<ScreenManager>().closeScreenManager();
				if(_gameManager.challenge != null)
				{
					LevelHandler levelhandler = LevelHandler.instance;
					//print (levelhandler._gameOverParameter);
					if(levelhandler._gameOverParameter == LevelHandler.GameoverParameter.ball)
					{
						GameObject.Find("ScreenManager").GetComponent<ScreenManager>().LoadScreen("ArcadeGamePlay");
						
					}
					
					if(levelhandler._gameOverParameter == LevelHandler.GameoverParameter.time)
					{
						levelhandler.StartTimer();
						GameObject.Find("ScreenManager").GetComponent<ScreenManager>().LoadScreen("SkillShot");
						
					}
					
					EventManager.GameStartTrigger();
				}
			}
		}
		
		
		
	}
	
	public void SetActiveChallengeScreen()
	{
		switch(_gameManager.GetCategory())
		{
			case GameManager.Category.HitTheBar:
			{
				//GameObject.Find("HitTheBar(Clone)").GetComponent<HittheBarGUIItemsManager>().SetActive(true);
				HittheBarGUIItemsManager.instance.SetActive(true);
				break;
			}
			
			case GameManager.Category.NoSwingStorm:
			{
				NoswingStormGUIItemsManager.instance.SetActive(true);
				break;
			}
			
			case GameManager.Category.ThroughRings:
			{
				ThroughTheRingsGUIItemsManager.instance.SetActive(true);
				break;
			}
		}
	}
}
using UnityEngine;
using System.Collections;

public class LoadingScreenGUIItemsManager : GUIItemsManager
{
	public GameObject MainBackGround;	
	public GameObject ring;
	public GameObject subscreenmanager;
	ScreenManager subscreenitemmanager;	
	public GameObject gametips;
	GameObject gearbuttondeactivated = null;
	private GameManager _gameManager;
	private InputManager _inputManager;
	public Texture2D practiceInstruction;
	public Texture2D SuddendeathInstruction;
	public Texture2D ClockShowDownInstruction;
	
	public GUITexture modeInstruction;
	bool tutorial_level = false;
	
	// Use this for initialization
	void Start ()
	{

		base.Init();	
		_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		_inputManager=GameObject.Find("GameManager").GetComponent("InputManager") as InputManager ;
		switch(_gameManager.GetGameMode())
		{
			case GameManager.GameMode.Training:
			{
				modeInstruction.texture = practiceInstruction;
				break;
			}
			
			case GameManager.GameMode.ClockShowDown:
			{
				modeInstruction.texture = ClockShowDownInstruction;
				break;
			}
			
			case GameManager.GameMode.SuddenDeath:
			{
				modeInstruction.texture = SuddendeathInstruction;
				break;
			}
				
		}
		
	}
//	void Update()
//	{
//		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
//			return;
//		if(Input.GetKeyDown(KeyCode.Escape))
//		{
//			_screenManager.LoadScreen("MainMenu");
//		}
//	}
	public void OnSubScreenSelectedEvent(GUIItem item)
	{
		if(meState != eGUI_ITEMS_MANAGER_STATE.Settled)
			return;
				
	}	
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
//			Debug.Log("----- In Derived class event handle : " + item.name);
			if(item.name == "SkipBtn")
			{
				GameManager.instance._inputManager.inputStatus = InputManager.InputStatus.idle;

				_screenManager.LoadScreen("Empty");
				//PlayerPrefs.SetString("tuetorial","tuetorialoff");
				if(_gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
				{
					_gameManager.LoadChallenge(_gameManager.GetIndex());
					PoleMovementRotation polemovement = _gameManager.GetChallenge().GetComponent<PoleMovementRotation>();
					if(polemovement!=null)
					{
						polemovement.Enable();
					}
			    }
			}
		
		}
	}	
	
	public override void OnExitAnimationCompleted()
	{
		
		EventManager.GameStartTrigger();
		SoundManager.instance.SoundStatefunction(SoundManager.soundstateInGame.gameplay);
		
	}
	void Update(){
		if (Input.GetKeyUp (KeyCode.Escape)) {

			_screenManager.LoadScreen("Empty");
			//PlayerPrefs.SetString("tuetorial","tuetorialoff");
			if(_gameManager.GetGameMode() == GameManager.GameMode.SkillShots)
			{
				_gameManager.LoadChallenge(_gameManager.GetIndex());
				PoleMovementRotation polemovement = _gameManager.GetChallenge().GetComponent<PoleMovementRotation>();
				if(polemovement!=null)
				{
					polemovement.Enable();
				}
			}
		}
	}
}
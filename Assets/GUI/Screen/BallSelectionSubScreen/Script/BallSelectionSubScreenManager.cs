﻿using UnityEngine;
using System.Collections;

public enum BALLSTATE
{
	BALLENABLED, BALLACTIVATED, BALLLOCKED
}

[System.Serializable]
public class ButtonElements
{
	public Texture2D BallLockedTexture,BallEnabledTexture, BallActivatedTexture;
	public GUIItemButton BallButton;
	public GUITexture BallTexture;
	public BALLSTATE _eBALLSTATE;
	public int CoinsNeededToUnlockBall, NumberOfBalls, BallsToBeAdded;
	public GameObject LockImage, GlowOutLine;
	public GUIText BallNumberText;
	public string PlayerPrefForStoring;
}

public class BallSelectionSubScreenManager : GUIItemsManager 
{		
	public ButtonElements _cButton1Elements, _cButton2Elements, _cButton3Elements, _cButton4Elements;
	ButtonElements _clickedBallIndex;
	int clickedButtonIndex;
	public static BallSelectionSubScreenManager instance;

	public enum BALLINDEX
	{
		BALL1, BALL2, BALL3, BALL4
	}
	public BALLINDEX _eBALLINDEX;
	
	public GameObject _gpopupsubscreen;
	
	//public GUIText _GUITextBallHeading;
	public string[] _ballHeadings;
	
	GameManager _sGameManager;
	InputManager _inputManager;
	BallHandler ballHandler;
	BallSelectionManager _sBallSelectionManager;

	void Awake()
	{
		GUITexture[] childTextures;
		GUIText[] childTexts;
	}
	// Use this for initialization
	void Start () 
	{
		instance = this;
		base.Init();

		_sGameManager = GameManager.GetInstance ();
		_inputManager = InputManager.GetInstance();
		ballHandler = GameObject.Find("GameManager").GetComponent<BallHandler>();
		_sBallSelectionManager = BallSelectionManager.GetInstance ();
		
		_cButton2Elements.NumberOfBalls = PlayerPrefs.GetInt(_cButton2Elements.PlayerPrefForStoring);
		_cButton3Elements.NumberOfBalls = PlayerPrefs.GetInt(_cButton3Elements.PlayerPrefForStoring);
		_cButton4Elements.NumberOfBalls = PlayerPrefs.GetInt(_cButton4Elements.PlayerPrefForStoring);
		
		makeTabs ();
		
		checkRugbyCoins (_sGameManager.GetCoins ());
        if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode) {
			//					ActivateSelectedBallLocal (4);
			setBall1Status (BALLSTATE.BALLACTIVATED);
			setBall1Status (BALLSTATE.BALLENABLED);
			setBall3Status (BALLSTATE.BALLENABLED);
			setBall2Status (BALLSTATE.BALLENABLED);
			setBall4Status (BALLSTATE.BALLENABLED);

			_cButton2Elements.BallNumberText.gameObject.SetActive (false);
			_cButton2Elements.LockImage.SetActive (false);
			_cButton3Elements.BallNumberText.gameObject.SetActive (false);
			_cButton3Elements.LockImage.SetActive (false);
			_cButton4Elements.BallNumberText.gameObject.SetActive (false);
			_cButton4Elements.LockImage.SetActive (false);


		} else {
			ActivateSelectedBallLocal (1);
		}

        setBall1Status(BALLSTATE.BALLACTIVATED);
        setBall2Status(BALLSTATE.BALLENABLED);
        setBall3Status(BALLSTATE.BALLENABLED);
        setBall4Status(BALLSTATE.BALLENABLED);
        _cButton2Elements.BallNumberText.gameObject.SetActive(false);
        _cButton2Elements.LockImage.SetActive(false);
        _cButton3Elements.BallNumberText.gameObject.SetActive(false);
        _cButton3Elements.LockImage.SetActive(false);
        _cButton4Elements.BallNumberText.gameObject.SetActive(false);
        _cButton4Elements.LockImage.SetActive(false);

	}
	
	void makeTabs ()
	{
		setAsTabButton("Ball1");
		setAsTabButton("Ball2");
		setAsTabButton("Ball3");
		setAsTabButton("Ball4");
		
		setTabButtonActive(0, true);
		setTabButtonActive(1, true);
		setTabButtonActive(2, true);
		setTabButtonActive(3, true);
		
		for(int i = 0 ; i< _sBallSelectionManager._ballType.Length;i++)
		{
			if(_sBallSelectionManager._ballType[i].isAvailable)
			{
				if(_sBallSelectionManager._ballType[i].isSelected)
				{
					if(i == 0)
					{
						setBall1Status(BALLSTATE.BALLACTIVATED);
						setBall2Status(BALLSTATE.BALLLOCKED);
						setBall3Status(BALLSTATE.BALLLOCKED);
						setBall4Status(BALLSTATE.BALLLOCKED);
					}
					else if(i == 1)
					{
						setBall2Status(BALLSTATE.BALLACTIVATED);
						setBall1Status(BALLSTATE.BALLENABLED);
						setBall3Status(BALLSTATE.BALLLOCKED);
						setBall4Status(BALLSTATE.BALLLOCKED);
					}
					else if(i == 2)
					{
						setBall3Status(BALLSTATE.BALLACTIVATED);
						setBall2Status(BALLSTATE.BALLLOCKED);
						setBall1Status(BALLSTATE.BALLENABLED);
						setBall4Status(BALLSTATE.BALLLOCKED);
					}
					else if(i == 3)
					{
						setBall4Status(BALLSTATE.BALLACTIVATED);
						setBall2Status(BALLSTATE.BALLLOCKED);
						setBall1Status(BALLSTATE.BALLENABLED);
						setBall3Status(BALLSTATE.BALLLOCKED);
					}
				}
			}
		}
	}
	
	void changestateofbuttonIndex(int buttonIndex, bool state)
	{
		switch (buttonIndex)
		{			
			case 1:
			//	print ("enable2ndBall");
				setBall2Status (BALLSTATE.BALLENABLED);
			break;
			
			case 2:
			//	print ("enable3rdBall");
				setBall3Status(BALLSTATE.BALLENABLED);
			break;
			
			case 3:
//				print ("enable4thBall");
				setBall4Status(BALLSTATE.BALLENABLED);
			break;
		}
	}

	void Update()
	{

	}
	
	public void setBall1Status (BALLSTATE state)
	{
		switch (state)
		{
			case BALLSTATE.BALLACTIVATED:
				setTabButtonActive(0, true);
				_cButton1Elements.BallButton._texNormalState = _cButton1Elements.BallActivatedTexture;
				_cButton1Elements.BallButton._texSelectedState = _cButton1Elements.BallActivatedTexture;
				_cButton1Elements.BallTexture.texture = _cButton1Elements.BallActivatedTexture;
				_cButton1Elements._eBALLSTATE = state;
				_eBALLINDEX = BALLINDEX.BALL1;
				
				BallSelectionManager.instance.UnlockBall (1);
				ballHandler.UpdateTexture();
                _cButton1Elements.GlowOutLine.SetActive(true);
			break;
			
			case BALLSTATE.BALLENABLED:
				setTabButtonActive(0, false);
				_cButton1Elements.BallButton._texNormalState = _cButton1Elements.BallEnabledTexture;
				_cButton1Elements.BallButton._texSelectedState = _cButton1Elements.BallEnabledTexture;
				_cButton1Elements.BallTexture.texture = _cButton1Elements.BallEnabledTexture;
				_cButton1Elements._eBALLSTATE = state;
                _cButton1Elements.GlowOutLine.SetActive(false);
			break;
			
			case BALLSTATE.BALLLOCKED:
				setTabButtonActive(0, false);
				_cButton1Elements.BallButton._texNormalState = _cButton1Elements.BallLockedTexture;
				_cButton1Elements.BallButton._texSelectedState = _cButton1Elements.BallLockedTexture;
				_cButton1Elements.BallTexture.texture = _cButton1Elements.BallLockedTexture;
				_cButton1Elements._eBALLSTATE = state;
                _cButton1Elements.GlowOutLine.SetActive(false);
			break;
		}
	}
	
	void setBall2Status (BALLSTATE state)
	{
		switch (state)
		{
			case BALLSTATE.BALLACTIVATED:	
				setTabButtonActive(1, true);
				_cButton2Elements.BallButton._texNormalState = _cButton2Elements.BallActivatedTexture;
				_cButton2Elements.BallButton._texSelectedState = _cButton2Elements.BallActivatedTexture;
				_cButton2Elements.BallTexture.texture = _cButton2Elements.BallActivatedTexture;
				_cButton2Elements._eBALLSTATE = state;
                _cButton2Elements.LockImage.SetActive(false);
                _cButton2Elements.BallNumberText.gameObject.SetActive(true);
                _cButton2Elements.GlowOutLine.SetActive(true);

				_cButton2Elements.BallNumberText.text = ""+_cButton2Elements.NumberOfBalls;
				_eBALLINDEX = BALLINDEX.BALL2;
				BallSelectionManager.instance.UnlockBall (2);
				ballHandler.UpdateTexture();
				PlayerPrefs.SetInt ("ball2Activated",1);
				_sGameManager.IncrementBallCountOfIndex (2);
				_sGameManager.updateAchievements (15);
			break;
			
			case BALLSTATE.BALLENABLED:
				setTabButtonActive(1, false);
				_cButton2Elements.BallButton._texNormalState = _cButton2Elements.BallEnabledTexture;
				_cButton2Elements.BallButton._texSelectedState = _cButton2Elements.BallEnabledTexture;
				_cButton2Elements.BallTexture.texture = _cButton2Elements.BallEnabledTexture;
				_cButton2Elements._eBALLSTATE = state;
                if (_cButton2Elements.NumberOfBalls == 0)
                {
                    _cButton2Elements.LockImage.SetActive(true);
                }
                else
                {
                    _cButton2Elements.LockImage.SetActive(false);
                    _cButton2Elements.BallNumberText.gameObject.SetActive(true);
                    _cButton2Elements.BallNumberText.text = "" + _cButton2Elements.NumberOfBalls;
                    _cButton2Elements.GlowOutLine.SetActive(false);
                }	
			break;
			
			case BALLSTATE.BALLLOCKED:
			_cButton2Elements.BallButton._texNormalState = _cButton2Elements.BallLockedTexture;
			_cButton2Elements.BallButton._texSelectedState = _cButton2Elements.BallLockedTexture;
			_cButton2Elements.BallTexture.texture = _cButton2Elements.BallLockedTexture;
			_cButton2Elements._eBALLSTATE = state;
                setTabButtonActive(1, false);
                if (_cButton2Elements.NumberOfBalls != 0)
                    setBall2Status(BALLSTATE.BALLENABLED);
                else
                {
                    _cButton2Elements.LockImage.SetActive(true);
                    _cButton2Elements.GlowOutLine.SetActive(false);

                }
			break;
		}	
	}
	
	void setBall3Status (BALLSTATE state)
	{
		switch (state)
		{
			case BALLSTATE.BALLACTIVATED:
				setTabButtonActive(2, true);
				_cButton3Elements.BallButton._texNormalState = _cButton3Elements.BallActivatedTexture;
				_cButton3Elements.BallButton._texSelectedState = _cButton3Elements.BallActivatedTexture;
				_cButton3Elements.BallTexture.texture = _cButton3Elements.BallActivatedTexture;
				_cButton3Elements._eBALLSTATE = state;
				
				_cButton3Elements.BallNumberText.text = ""+_cButton3Elements.NumberOfBalls;
				_eBALLINDEX = BALLINDEX.BALL3;
				
				BallSelectionManager.instance.UnlockBall (3);
				ballHandler.UpdateTexture();
				PlayerPrefs.SetInt ("ball3Activated",1);
				_sGameManager.IncrementBallCountOfIndex (3);
				_sGameManager.updateAchievements (15);
                _cButton3Elements.BallNumberText.gameObject.SetActive(true);
                _cButton3Elements.GlowOutLine.SetActive(true);
                _cButton3Elements.LockImage.SetActive(false);
			break;
			
			case BALLSTATE.BALLENABLED:
				setTabButtonActive(2, false);
				_cButton3Elements.BallButton._texNormalState = _cButton3Elements.BallEnabledTexture;
				_cButton3Elements.BallButton._texSelectedState = _cButton3Elements.BallEnabledTexture;
				_cButton3Elements.BallTexture.texture = _cButton3Elements.BallEnabledTexture;
				_cButton3Elements._eBALLSTATE = state;
				
				_cButton3Elements.BallNumberText.text = ""+_cButton3Elements.NumberOfBalls;
                if (_cButton3Elements.NumberOfBalls == 0)
                {
                    _cButton3Elements.LockImage.SetActive(true);
                }
                else
                {
                    _cButton3Elements.LockImage.SetActive(false);
                    _cButton3Elements.BallNumberText.gameObject.SetActive(true);
                    _cButton3Elements.GlowOutLine.SetActive(false);
                }
			break;
			
			case BALLSTATE.BALLLOCKED:
				setTabButtonActive(2, false);
				_cButton3Elements.BallButton._texNormalState = _cButton3Elements.BallLockedTexture;
				_cButton3Elements.BallButton._texSelectedState = _cButton3Elements.BallLockedTexture;
				_cButton3Elements.BallTexture.texture = _cButton3Elements.BallLockedTexture;
				_cButton3Elements._eBALLSTATE = state;
                if (_cButton3Elements.NumberOfBalls != 0)
                {
                    setBall3Status(BALLSTATE.BALLENABLED);
                }
                else
                {
                    _cButton3Elements.LockImage.SetActive(true);
                    _cButton3Elements.GlowOutLine.SetActive(false);
                }
			break;
		}	
	}
	
	void setBall4Status (BALLSTATE state)
	{
		switch (state)
		{
			case BALLSTATE.BALLACTIVATED:
				setTabButtonActive(3, true);
				_cButton4Elements.BallButton._texNormalState = _cButton4Elements.BallActivatedTexture;
				_cButton4Elements.BallButton._texSelectedState = _cButton4Elements.BallActivatedTexture;
				_cButton4Elements.BallTexture.texture = _cButton4Elements.BallActivatedTexture;
				_cButton4Elements._eBALLSTATE = state;
				
				_cButton4Elements.BallNumberText.text = ""+_cButton4Elements.NumberOfBalls;
				_eBALLINDEX = BALLINDEX.BALL4;
				
//				print ("Instantiate 4th Ball");
				BallSelectionManager.instance.UnlockBall (4);
				ballHandler.UpdateTexture();
				PlayerPrefs.SetInt ("ball4Activated",1);
				_sGameManager.IncrementBallCountOfIndex (4);
				_sGameManager.updateAchievements (15);
                _cButton4Elements.BallNumberText.gameObject.SetActive(true);
                _cButton4Elements.GlowOutLine.SetActive(true);
                _cButton4Elements.LockImage.SetActive(false);
     			break;
			
			case BALLSTATE.BALLENABLED:
				setTabButtonActive(3, false);
				_cButton4Elements.BallButton._texNormalState = _cButton4Elements.BallEnabledTexture;
				_cButton4Elements.BallButton._texSelectedState = _cButton4Elements.BallEnabledTexture;
				_cButton4Elements.BallTexture.texture = _cButton4Elements.BallEnabledTexture;
				_cButton4Elements._eBALLSTATE = state;
                if (_cButton4Elements.NumberOfBalls == 0)
                {
                    _cButton4Elements.LockImage.SetActive(true);
                }
                else
                {
                    _cButton4Elements.LockImage.SetActive(false);
                    _cButton4Elements.BallNumberText.gameObject.SetActive(true);
                    _cButton4Elements.BallNumberText.text = "" + _cButton4Elements.NumberOfBalls;
                    _cButton4Elements.GlowOutLine.SetActive(false);
                }
			break;
			
			case BALLSTATE.BALLLOCKED:
				setTabButtonActive(3, false);
				_cButton4Elements.BallButton._texNormalState = _cButton4Elements.BallLockedTexture;
				_cButton4Elements.BallButton._texSelectedState = _cButton4Elements.BallLockedTexture;
				_cButton4Elements.BallTexture.texture = _cButton4Elements.BallLockedTexture;
				_cButton4Elements._eBALLSTATE = state;
                if (_cButton4Elements.NumberOfBalls != 0)
                {
                    setBall4Status(BALLSTATE.BALLENABLED);
                }
                else
                {
                    _cButton4Elements.LockImage.SetActive(true);
                    _cButton4Elements.GlowOutLine.SetActive(false);

                }
			break;
		}	
	}
	
	public void checkRugbyCoins (int coins)
	{
		if(coins >= _cButton2Elements.CoinsNeededToUnlockBall && _cButton2Elements._eBALLSTATE == BALLSTATE.BALLLOCKED)
			changestateofbuttonIndex (1, false);
		
		if(coins >= _cButton3Elements.CoinsNeededToUnlockBall && _cButton3Elements._eBALLSTATE == BALLSTATE.BALLLOCKED)
			changestateofbuttonIndex (2, false);
		
		if(coins >= _cButton4Elements.CoinsNeededToUnlockBall && _cButton4Elements._eBALLSTATE == BALLSTATE.BALLLOCKED)
			changestateofbuttonIndex (3, false);
	}
		
	public void checkRugbyCoinsPublic ()
	{
		checkRugbyCoins (_sGameManager.GetCoins ());
	}
	
	void changeButtonStateFromActivation (int ButtonIndex)
	{		
		checkRugbyCoins (_sGameManager.GetCoins ());
		
		switch (ButtonIndex)
		{
			case 1:
				setBall1Status(BALLSTATE.BALLENABLED);
			break;
			
			case 2:
				if(_cButton2Elements.NumberOfBalls == 0 && _sGameManager.GetCoins () < _cButton2Elements.CoinsNeededToUnlockBall)
					setBall2Status(BALLSTATE.BALLLOCKED);
				else
					setBall2Status(BALLSTATE.BALLENABLED);	
			break;
			
			case 3:
				if(_cButton3Elements.NumberOfBalls == 0 && _sGameManager.GetCoins () < _cButton3Elements.CoinsNeededToUnlockBall)
					setBall3Status(BALLSTATE.BALLLOCKED);
				else
					setBall3Status(BALLSTATE.BALLENABLED);	
			break;
			
			case 4:
				if(_cButton4Elements.NumberOfBalls == 0 && _sGameManager.GetCoins () < _cButton4Elements.CoinsNeededToUnlockBall)
					setBall4Status(BALLSTATE.BALLLOCKED);
				else
					setBall4Status(BALLSTATE.BALLENABLED);	
			break;
		}
	}
		
	public override void OnSelectedEvent(GUIItem item)
	{
		if(meState == eGUI_ITEMS_MANAGER_STATE.Settled && _screenManager != null)
		{
			//Debug.Log("----- In Derived class event handle : " + item.name);
			
			if(item.name == "Ball1")
			{
				if(_inputManager.GetInputStatus() != InputManager.InputStatus.idle)
					return ;
				/*if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode) {
					switch (_cButton1Elements._eBALLSTATE) {
					case BALLSTATE.BALLACTIVATED:
					
						break;
					
					case BALLSTATE.BALLENABLED:
						if (_sGameManager.GetCoins () >= _cButton1Elements.CoinsNeededToUnlockBall) {
							setBall1Status (BALLSTATE.BALLACTIVATED);
							
							_sGameManager.AddCoin (-_cButton1Elements.CoinsNeededToUnlockBall);
						
							changeButtonStateFromActivation (2);
							changeButtonStateFromActivation (3);
							changeButtonStateFromActivation (4);
							clickedButtonIndex = 1;
						}
						break;
					
					case BALLSTATE.BALLLOCKED:
						showNotEnoughCoins ();
						_clickedBallIndex = _cButton1Elements;
						clickedButtonIndex = 1;
						break;
					}
				}*/
				//if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.M_Sports_Mode) {
					setBall1Status(BALLSTATE.BALLACTIVATED);
					//Debug.Log ("Ball1");
					setBall2Status(BALLSTATE.BALLENABLED);
					setBall3Status(BALLSTATE.BALLENABLED);
					setBall4Status(BALLSTATE.BALLENABLED);
					_cButton2Elements.BallNumberText.gameObject.SetActive(false);
					_cButton2Elements.LockImage.SetActive(false);
					_cButton3Elements.BallNumberText.gameObject.SetActive(false);
					_cButton3Elements.LockImage.SetActive(false);
					_cButton4Elements.BallNumberText.gameObject.SetActive(false);
					_cButton4Elements.LockImage.SetActive(false);
				//}
			}

			else if(item.name == "Ball2")
			{
				
				if(_inputManager.GetInputStatus() != InputManager.InputStatus.idle)
					return ;
				/*if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode) {
					switch (_cButton2Elements._eBALLSTATE) {

					case BALLSTATE.BALLACTIVATED:
						Debug.Log ("BALLACTIVATED");
						break;
					
				case BALLSTATE.BALLENABLED:
					//Debug.Log ("BALLENABLED");
					if (_cButton2Elements.NumberOfBalls == 0) {
						//Debug.Log ("NumberOfBalls");
						_clickedBallIndex = _cButton2Elements;
						_screenManager.DisableInput ();
						
						Instantiate (_gpopupsubscreen);
						   
						  
						clickedButtonIndex = 2;
					} else {
						ActivateSelectedBallLocal (2);
						//Debug.Log ("ActivateSelectedBallLocal");
					}
						break;
					
					case BALLSTATE.BALLLOCKED:
						//Debug.Log ("BALLLOCKED");
						showNotEnoughCoins ();
						_clickedBallIndex = _cButton2Elements;
						clickedButtonIndex = 2;
						break;
					}
				}*/
				//if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.M_Sports_Mode) {
//					ActivateSelectedBallLocal (2);
					setBall2Status(BALLSTATE.BALLACTIVATED);
					setBall1Status(BALLSTATE.BALLENABLED);
					setBall3Status(BALLSTATE.BALLENABLED);
					setBall4Status(BALLSTATE.BALLENABLED);
					_cButton2Elements.BallNumberText.gameObject.SetActive(false);
					_cButton2Elements.LockImage.SetActive(false);
					_cButton3Elements.BallNumberText.gameObject.SetActive(false);
					_cButton3Elements.LockImage.SetActive(false);
					_cButton4Elements.BallNumberText.gameObject.SetActive(false);
					_cButton4Elements.LockImage.SetActive(false);
				//}
			}
			
			else if(item.name == "Ball3")
			{
				if(_inputManager.GetInputStatus() != InputManager.InputStatus.idle)
					return ;
				/*if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode) {
					switch (_cButton3Elements._eBALLSTATE) {
				case BALLSTATE.BALLACTIVATED:
//					Debug.Log ("BALLACTIVATED");
						break;
					
					case BALLSTATE.BALLENABLED:
						if (_cButton3Elements.NumberOfBalls == 0) {
							_clickedBallIndex = _cButton3Elements;
							_screenManager.DisableInput ();
							Instantiate (_gpopupsubscreen);
						   
						  
							clickedButtonIndex = 3;
						} else
							ActivateSelectedBallLocal (3);
						break;
					
					case BALLSTATE.BALLLOCKED:
						showNotEnoughCoins ();
						_clickedBallIndex = _cButton3Elements;
						clickedButtonIndex = 3;
						break;
					}
				}*/
				//if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.M_Sports_Mode) {
//					ActivateSelectedBallLocal (3);
					setBall3Status(BALLSTATE.BALLACTIVATED);
					setBall1Status(BALLSTATE.BALLENABLED);
					setBall2Status(BALLSTATE.BALLENABLED);
					setBall4Status(BALLSTATE.BALLENABLED);
					_cButton2Elements.BallNumberText.gameObject.SetActive(false);
					_cButton2Elements.LockImage.SetActive(false);
					_cButton3Elements.BallNumberText.gameObject.SetActive(false);
					_cButton3Elements.LockImage.SetActive(false);
					_cButton4Elements.BallNumberText.gameObject.SetActive(false);
					_cButton4Elements.LockImage.SetActive(false);
				//}
			}
			
			else if(item.name == "Ball4")
			{
				if(_inputManager.GetInputStatus() != InputManager.InputStatus.idle)
					return ;
				/*if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.NormalMode) {
					switch (_cButton4Elements._eBALLSTATE) {
					case BALLSTATE.BALLACTIVATED:
					
						break;
					
					case BALLSTATE.BALLENABLED:
						if (_cButton4Elements.NumberOfBalls == 0) {
							_clickedBallIndex = _cButton4Elements;
							_screenManager.DisableInput ();
							Instantiate (_gpopupsubscreen);
						   
						 
							clickedButtonIndex = 4;
						} else
							ActivateSelectedBallLocal (4);
						break;
					
					case BALLSTATE.BALLLOCKED:
						showNotEnoughCoins ();
						_clickedBallIndex = _cButton4Elements;
						clickedButtonIndex = 4;
						break;
					}
				}*/
				//if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.M_Sports_Mode) {
//					ActivateSelectedBallLocal (4);
					setBall4Status(BALLSTATE.BALLACTIVATED);
					setBall1Status(BALLSTATE.BALLENABLED);
					setBall3Status(BALLSTATE.BALLENABLED);
					setBall2Status(BALLSTATE.BALLENABLED);
					_cButton2Elements.BallNumberText.gameObject.SetActive(false);
					_cButton2Elements.LockImage.SetActive(false);
					_cButton3Elements.BallNumberText.gameObject.SetActive(false);
					_cButton3Elements.LockImage.SetActive(false);
					_cButton4Elements.BallNumberText.gameObject.SetActive(false);
					_cButton4Elements.LockImage.SetActive(false);
				//}
			}
		}
	}
	
	void showNotEnoughCoins ()
	{
		print ("Not Enoug Coins... Instantiate CoinSubScreen");

			_screenManager.DisableInput ();
			Instantiate (_gpopupsubscreen);
	
	}
	
	
	
//	void OnGUI ()
//	{
//		if(GUI.Button(new Rect(10, 10, 100, 50), ""+_mcoins))
//		{
//			///_mcoins += 3;
//			//checkRugbyCoins (_mcoins);
//		}
//		
//		if(GUI.Button (new Rect(10, 60, 100, 50), "ReduceBall"))
//		{
//			ReduceBallCount ();
//		}
//	}
	
	public void ReduceBallCount ()
	{
		switch (_eBALLINDEX)
		{
			case BALLINDEX.BALL1:
				
			break;
			
			case BALLINDEX.BALL2:
				if(_cButton2Elements.NumberOfBalls > 0)
				{
					_cButton2Elements.NumberOfBalls --;
					PlayerPrefs.SetInt(_cButton2Elements.PlayerPrefForStoring, _cButton2Elements.NumberOfBalls);
					BallSelectionManager.instance._ballType[1].noOfBallAvailable--;
					_cButton2Elements.BallNumberText.text = ""+_cButton2Elements.NumberOfBalls;
					if(_cButton2Elements.NumberOfBalls == 0)
					{
						changeButtonStateFromActivation (2);
						setBall1Status(BALLSTATE.BALLACTIVATED);
						checkRugbyCoins (_sGameManager.GetCoins ());
					}
				}
			break;
			
			case BALLINDEX.BALL3:
				if(_cButton3Elements.NumberOfBalls > 0)
				{
					_cButton3Elements.NumberOfBalls --;
					PlayerPrefs.SetInt(_cButton3Elements.PlayerPrefForStoring, _cButton3Elements.NumberOfBalls);
					BallSelectionManager.instance._ballType[2].noOfBallAvailable--;
					_cButton3Elements.BallNumberText.text = ""+_cButton3Elements.NumberOfBalls;
					if(_cButton3Elements.NumberOfBalls == 0)
					{
						changeButtonStateFromActivation (3);
						setBall1Status(BALLSTATE.BALLACTIVATED);
						checkRugbyCoins (_sGameManager.GetCoins ());
					}
				}
			break;
			
			case BALLINDEX.BALL4:
				if(_cButton4Elements.NumberOfBalls > 0)
				{
					_cButton4Elements.NumberOfBalls --;
					PlayerPrefs.SetInt(_cButton4Elements.PlayerPrefForStoring, _cButton4Elements.NumberOfBalls);
					BallSelectionManager.instance._ballType[3].noOfBallAvailable--;
					_cButton4Elements.BallNumberText.text = ""+_cButton4Elements.NumberOfBalls;
					if(_cButton4Elements.NumberOfBalls == 0)
					{
						changeButtonStateFromActivation (4);
						setBall1Status(BALLSTATE.BALLACTIVATED);
						checkRugbyCoins (_sGameManager.GetCoins ());
					}
				}
			break;
		}
	}
	
	public int selectedBallCoinStatus (int index)
	{
		if(index == 1)
			return _sGameManager.GetCoins ();
		else if(index == 2)
			return _clickedBallIndex.CoinsNeededToUnlockBall;
		else if(index == 3)
			return _clickedBallIndex.BallsToBeAdded;
		else
			return clickedButtonIndex;
	}
	
	public void enableScreenClosingPopup ()
	{
		_screenManager.EnableInput ();
	}
	
	void ActivateSelectedBallLocal (int Ballindex)
	{
		if(Ballindex == 1)
		{
			setBall1Status(BALLSTATE.BALLACTIVATED);
				
			changeButtonStateFromActivation (2);
			changeButtonStateFromActivation (3);
			changeButtonStateFromActivation (4);

			checkRugbyCoins (_sGameManager.GetCoins ());
		}
		else if(Ballindex == 2)
		{
			setBall2Status(BALLSTATE.BALLACTIVATED);
							
			changeButtonStateFromActivation (1);
			changeButtonStateFromActivation (3);
			changeButtonStateFromActivation (4);
		
			checkRugbyCoins (_sGameManager.GetCoins ());
		}
		
		else if(Ballindex == 3)
		{
			setBall3Status(BALLSTATE.BALLACTIVATED);

			changeButtonStateFromActivation (1);
			changeButtonStateFromActivation (2);
			changeButtonStateFromActivation (4);
		
			checkRugbyCoins (_sGameManager.GetCoins ());
		}
		
		else if(Ballindex == 4)
		{
			setBall4Status(BALLSTATE.BALLACTIVATED);
										
			changeButtonStateFromActivation (1);
			changeButtonStateFromActivation (2);
			changeButtonStateFromActivation (3);
		
			checkRugbyCoins (_sGameManager.GetCoins ());
		}
	}
	
	public void ActivateSelectedBall (int Ballindex)
	{
		if(Ballindex == 2)
		{			
			if (_sGameManager.GetCoins () >= _cButton2Elements.CoinsNeededToUnlockBall) {
				_sGameManager.AddCoin (-_cButton2Elements.CoinsNeededToUnlockBall);

				_cButton2Elements.NumberOfBalls += _cButton2Elements.BallsToBeAdded;
				
				PlayerPrefs.SetInt (_cButton2Elements.PlayerPrefForStoring, _cButton2Elements.NumberOfBalls);
			} 
			else if (BallSelectionPopup.VideoClicked == true) {

				_cButton2Elements.NumberOfBalls += _cButton2Elements.BallsToBeAdded;

				PlayerPrefs.SetInt (_cButton2Elements.PlayerPrefForStoring, _cButton2Elements.NumberOfBalls);
				BallSelectionPopup.VideoClicked = false;

			}
			setBall2Status(BALLSTATE.BALLACTIVATED);
							
			changeButtonStateFromActivation (1);
			changeButtonStateFromActivation (3);
			changeButtonStateFromActivation (4);
		
			checkRugbyCoins (_sGameManager.GetCoins ());
		}	
		
		else if(Ballindex == 3)
		{
			if(_sGameManager.GetCoins () >= _cButton3Elements.CoinsNeededToUnlockBall)
			{
				_sGameManager.AddCoin(-_cButton3Elements.CoinsNeededToUnlockBall);
													
				_cButton3Elements.NumberOfBalls += _cButton3Elements.BallsToBeAdded;
				
				PlayerPrefs.SetInt(_cButton3Elements.PlayerPrefForStoring, _cButton3Elements.NumberOfBalls);
			}
			else if (BallSelectionPopup.VideoClicked == true) {
				_cButton3Elements.NumberOfBalls += _cButton3Elements.BallsToBeAdded;

				PlayerPrefs.SetInt(_cButton3Elements.PlayerPrefForStoring, _cButton3Elements.NumberOfBalls);
				BallSelectionPopup.VideoClicked = false;
			}
			setBall3Status(BALLSTATE.BALLACTIVATED);

			changeButtonStateFromActivation (1);
			changeButtonStateFromActivation (2);
			changeButtonStateFromActivation (4);
		
			checkRugbyCoins (_sGameManager.GetCoins ());
		}
		
		else if(Ballindex == 4)
		{
			if(_sGameManager.GetCoins () >= _cButton4Elements.CoinsNeededToUnlockBall)
			{
				_sGameManager.AddCoin(-_cButton4Elements.CoinsNeededToUnlockBall);
			
				_cButton4Elements.NumberOfBalls += _cButton4Elements.BallsToBeAdded;
				
				PlayerPrefs.SetInt(_cButton4Elements.PlayerPrefForStoring, _cButton4Elements.NumberOfBalls);
			}
			else if (BallSelectionPopup.VideoClicked == true) {
				_cButton4Elements.NumberOfBalls += _cButton4Elements.BallsToBeAdded;

				PlayerPrefs.SetInt(_cButton4Elements.PlayerPrefForStoring, _cButton4Elements.NumberOfBalls);
				BallSelectionPopup.VideoClicked = false;
			}
			setBall4Status(BALLSTATE.BALLACTIVATED);
										
			changeButtonStateFromActivation (1);
			changeButtonStateFromActivation (2);
			changeButtonStateFromActivation (3);
		
			checkRugbyCoins (_sGameManager.GetCoins ());
		}
	}
	public override void OnExitAnimationCompleted()
	{
        if (GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode) {
			ActivateSelectedBallLocal(1);
		}

	}
	public void AddCoinsToBallSelection (int CoinIndex)
	{
		switch (CoinIndex)
		{
			case 1:	
				_sGameManager.AddCoin(10);
			break;
			
			case 2:
				_sGameManager.AddCoin(20);
			break;
			
			case 3:
				_sGameManager.AddCoin(30);
			break;
			
			case 4:
				_sGameManager.AddCoin(40);
			break;
			
			case 5:
				_sGameManager.AddCoin(50);
			break;
			
			case 6:
				_sGameManager.AddCoin(60);
			break;
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneHandler : MonoBehaviour {

	public enum eGameStates
	{
		None,
		NormalMode,
		Sports_Mode
	};
	public eGameStates _eGameStates;
	public static GameSceneHandler Instance;
	public bool   _AdsEnable = false;
	// Use this for initialization
	void Awake () {

		Instance = this;

        if(_eGameStates != eGameStates.Sports_Mode)	
			SportsConstants._SportsMenuLoadCount = 0;
		else
			Invoke("UpdateValue",1);



		if(_AdsEnable)
		{
			if(!SportsConstants._AdsLoaded && Application.loadedLevel != 0)
			{
				SportsConstants._AdsLoaded = true;
				//Debug.Log("Unity Initialize");
			}
		}
		else
		{
			if(SportsConstants._AdsLoaded)
			{
				SportsConstants._AdsLoaded = false;
				//Debug.Log("Unity DiInitialize");
			}
		}







	}
	void Start()
	{
		Invoke("ResetScore",0.1f);
	}
	void UpdateValue()
	{
		SportsConstants._SportsMenuLoadCount ++;
	}
	// Update is called once per frame
	void Update () {

	}
	void ResetScore()
	{
        if(GameSceneHandler.Instance._eGameStates == eGameStates.Sports_Mode)
		{
			ScoreManager.instance.currentScore =0;
			ScoreManager.instance.ScoreOfCurrentShot = 0;
			//ScoreManager.miScoreOfOneShot = 0;
			//	ScoreManager.miCurveangelBonus = 0;
			//	ScoreManager.miDistanceBonus = 0;
			//	ScoreManager.miWindSpeedBonus = 0;
			//	ScoreManager.noOfgoal = 0;
			//	ScoreManager.noOfTokens = 0;
			ScoreManager.instance.modeScore = 0;
		}



	}
}

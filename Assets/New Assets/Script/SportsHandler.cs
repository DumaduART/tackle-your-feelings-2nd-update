﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MSportsHandler : MonoBehaviour {
	
	static double score = 0.0;
	public static MSportsHandler Instance;
	bool mLowScoreReplayOption = false;
	public bool _PlayAgainLoad = false;

	void Awake() {


		//Debug.Log("Script is loaded. Setting delegate");
		Instance = this;

		
	}
	// Use this for initialization
	void Start () {
		
        if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode)
		{
			if(!SportsConstants._GUILoad)
				mSportsInit();
		}
		else
		{
		//	if(MSportsConstants._GUILoad)
			DeInit();
			SportsConstants._GUILoad = false;
		}
        if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode)
		{
			if(!SportsConstants._GUILoad)
				mSportsShow ();
			SportsConstants._GUILoad = true;
			//Camera.main.GetComponent<AudioListener>().enabled = false;
		}
		else 
		{
			
		}

	//	LoadScene();
	
	}
	
	public void mSportsInit () {


		//Debug.Log("Calling SportsUnity.Init");	

	}

	public void mSportsShow () {
		//Debug.Log ("Calling SportsUnity.Show");

	//	MSportsUnity.GameStarted();
		//MSportsUnity.
	}

	public void mSportsGameStarted () {
		//Debug.Log ("Calling SportsUnity.GameStarted");
	
	}

	public void mSportsPostScore (int pTotalScore) {
		//Debug.Log ("Calling SportsUnity.PostScore");

		score = pTotalScore;

	}


	//IMSportsDelegate implementation

	public void MSportsPlay() {



		//Debug.Log ("SportsDelegate: MSportsPlay");
        if(GameSceneHandler.Instance._eGameStates == GameSceneHandler.eGameStates.Sports_Mode)
		{
			//mSportsGameStarted();
		}
		LoadScene();
	   
		//Invoke("LoadScene",1);
	//	Application.LoadLevel(2);
		//This method is invoked by MSports SDK when gameplay should be started.

		//Start the gameplay here, 
		//Call  MSportsUnity.GameStarted()
		//Let user play
		//Post score when gameplay is finished

	}

	public void MSportsShown() {
		

		//Debug.Log ("SportsDelegate: SportsShown");
		//Debug.Log ("Ravi Ranjan SportsShown");
		Camera.main.GetComponent<AudioSource>().volume  = 0;
		if(GameObject.Find("MainMenu(Clone)"))
		GameObject.Find("MainMenu(Clone)").GetComponent<MainScreenGUIItemsManager>().PlayButtonDisable();
		if(mLowScoreReplayOption)
		{
			GameObject.Find("TimeAttackGameOver(Clone)").GetComponent<TimeAttackOverGUIItemsManager>().DisableButton();
			_PlayAgainLoad = false;
		}
		#if UNITY_IPHONE 
		if(GameObject.Find("TimeAttackGamePlay(Clone)") || GameObject.Find("3(Clone)") || GameObject.Find("2(Clone)") || GameObject.Find("1(Clone)") || GameObject.Find("Go(Clone)"))
			Time.timeScale = 0;
		#endif
		//Application.LoadLevel(Application.loadedLevel);
		//This method is invoked when MSports UI becomes visible
		//Pause game and game music
	}

	public void MSportsHidden(){
		//Debug.Log ("SportsDelegate: SportsHidden");
		//Debug.Log ("Ravi Ranjan SportsHidden");
		Camera.main.GetComponent<AudioSource>().volume  = 1;
		if(mLowScoreReplayOption)
		{
			GameObject.Find("TimeAttackGameOver(Clone)").GetComponent<TimeAttackOverGUIItemsManager>().PlayButtonEnable();
			_PlayAgainLoad = true;
		}
		#if UNITY_IPHONE
		Time.timeScale = 1;
		#endif
		//This method is invoked when MSports UI becomes hidden
		//Gameplay and game music can be resumed here
	}
	public void DeInit()
	{
		
	}
	public void LoadScene()
	{


		if(GameObject.Find("MainMenu(Clone)"))
		{
			GameObject.Find("MainMenu(Clone)").GetComponent<MainScreenGUIItemsManager>().PlayButtonEnable();
		
		}

		if(	SportsConstants._SportsMenuLoadCount == 0)
		{
			if(GameObject.Find("MainMenu(Clone)"))
			{
				GameObject.Find("MainMenu(Clone)").GetComponent<MainScreenGUIItemsManager>().PlayButtonEnable();
			
			}
		}
		else
		{
			if(GameObject.Find("MainMenu(Clone)"))
			{
				GameObject.Find("MainMenu(Clone)").GetComponent<MainScreenGUIItemsManager>().PlayButtonEnable();
			
			}
			else if(GameObject.Find("TimeAttackGameOver(Clone)"))
			{
				#if UNITY_IPHONE
			
				if((Time.time - GameObject.Find("TimeAttackGameOver(Clone)").GetComponent<TimeAttackOverGUIItemsManager>()._GameOverTime) > 4)
				{
				CancelInvoke();
				Application.LoadLevel(Application.loadedLevel);
				}
				else
				{
				mLowScoreReplayOption = true;
				_PlayAgainLoad = true;
				GameObject.Find("TimeAttackGameOver(Clone)").GetComponent<TimeAttackOverGUIItemsManager>().EnableButtton();
				}

				#elif UNITY_ANDROID

				if((Time.realtimeSinceStartup - GameObject.Find("TimeAttackGameOver(Clone)").GetComponent<TimeAttackOverGUIItemsManager>()._GameOverTime) > 4)
				{
					CancelInvoke();
					Application.LoadLevel(Application.loadedLevel);
				}
				else
				{
					mLowScoreReplayOption = true;
					_PlayAgainLoad = true;
					GameObject.Find("TimeAttackGameOver(Clone)").GetComponent<TimeAttackOverGUIItemsManager>().EnableButtton();
				}


				#endif
			

			}
//			else 
//			{
//				Application.LoadLevel(Application.loadedLevel);
//			}
		}
	}
	public void ShowGUI()
	{
		//GameObject.Find("MainMenu").GetComponent<MainScreenGUIItemsManager>()._GuiIndicator.DisableIndicator();
	}
	public void MSportsPostScore(){
		
		//Debug.Log ("SportsDelegate: SportsPostScore");
		//Debug.Log ("Ravi Ranjan SportsPostScore");

		//This method is invoked when tournament round is about to end.
		//You can post user's current score before round ends

		//mSportsPostScore (SuddenDeath.instance.GetScore());  // if TimeAttack mode for MSport then mSportsPostScore (TimeAttack.instance.GetScore()); 
		GameManager.instance.mainScreenManager.LoadScreen("TimeAttackGameOver");                                                  // if SuddenDeath mode for MSport then mSportsPostScore (SuddenDeath.instance.GetScore()); 
	//	Application.LoadLevel(Application.loadedLevel);

	

	}
	public void  MSportsError(string pStates)
	{
		//Debug.Log ("pStates  " +pStates+ "End");
		SportsConstants._GUILoad = false;
		DeInit();
		Application.LoadLevel(1);
	}
}

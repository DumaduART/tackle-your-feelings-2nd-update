﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FBKit;
public class TestOnline : MonoBehaviour {
		
	public GameObject _TableView;
	GameObject mTableview = null;
	// Use this for initialization
	void Start () 
	{
//		Instantiate(_TableView);
//		Debug.Log("Start");
//		OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.LOGIN,null,CallBack);
	}
	void CallBack(string strResult ,eONLINE_REQ_TYPE eRequestType,IList<UserData> data = null)
	{
		//Debug.Log("Test CallBack"+strResult);
		if(eRequestType == eONLINE_REQ_TYPE.GET_TOP_RANKING_LIST || eRequestType == eONLINE_REQ_TYPE.GET_TOP_N_RANKING_LIST)
		{
			//Debug.Log("==Create Table View==");
			if(mTableview != null)
			{
				Destroy(mTableview);
				mTableview = null;
			}
			if(mTableview == null)
				mTableview =(GameObject)Instantiate(_TableView);
			JSONObject tJSONObj = new JSONObject();
			tJSONObj.type = JSONObject.Type.ARRAY;
			for(int i=0; i<data.Count;i++)
			{
				JSONObject tJSONObj2 = new JSONObject();
				tJSONObj2.AddField("Name",data[i]._UserName);
				tJSONObj2.AddField("Image",(data[i]._UserImageURL).ToString());
				tJSONObj2.AddField("UserId",data[i]._UserId);
				tJSONObj2.AddField("Score",data[i]._UserScore.ToString());
				tJSONObj2.AddField("Installed",true);
				tJSONObj.list.Add(tJSONObj2);
			}
			mTableview.GetComponent<FBTableViewDelegate>().FillTableViewWithData(tJSONObj.print(),0);
		}
		else if(eRequestType == eONLINE_REQ_TYPE.LOGOUT)
		{
			if(mTableview !=null)
			{
				Destroy(mTableview);
				mTableview = null;
			}
		}
	}
	
	void Invite()
	{
		string FriendSelectorTitle = "Invitation";
   		string FriendSelectorMessage = "Play this game";
    	string FriendSelectorFilters = "[\"all\",\"app_users\",\"app_non_users\"]";
    	string FriendSelectorData = "{\"Life\":\"10\"}";
    	string FriendSelectorMax = "150";	
		string FriendSelectorId = "";	
		
		JSONObject jObj = new JSONObject();
		jObj.AddField("message",FriendSelectorMessage);
		jObj.AddField("filters",FriendSelectorFilters);
		jObj.AddField("maxRecipients",FriendSelectorMax);
		jObj.AddField("title",FriendSelectorTitle);
		jObj.AddField("data",FriendSelectorData);
		jObj.AddField("id",FriendSelectorId);
		
		OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.INVITE_FRIENDS,jObj.print(),null);
	}
	
	void Share()
	{
		string FeedLink = "https://www.dumadugames.com";
		string FeedLinkName = "Dumadu Games";
		string FeedLinkCaption = "Check out this cool Game";
		string FeedLinkDescription = "Online Interface Handler from dumadu games.";
		
		JSONObject jObj = new JSONObject();
		jObj.AddField("link",FeedLink);
		jObj.AddField("linkName",FeedLinkName);
		jObj.AddField("linkCaption",FeedLinkCaption);
		jObj.AddField("linkDescription",FeedLinkDescription);
		
		OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.SHARE_ON_WALL,jObj.print(),null);
	}
	void OnGUI()
	{
		if(GUI.Button(new Rect(0,0,100,50),"Login"))
		{
			//Debug.Log("Login");
			OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.LOGIN,"",CallBack);
		}
		else if(GUI.Button(new Rect(105,0,100,50),"Logout"))
		{
			//Debug.Log("Login");
			OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.LOGOUT,"",CallBack);
		}
		else if(GUI.Button(new Rect(0,51,100,50),"Invite"))
		{
			//Debug.Log("Invite");
			Invite();
		}
		else if(GUI.Button(new Rect(0,102,100,50),"Share"))
		{
			//Debug.Log("Share");
			Share();
		}
		else if(GUI.Button(new Rect(0,153,100,50),"SaveScore"))
		{
			//Debug.Log("SaveScore");
			OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.UPDATE_VALUE_TO_SERVER,"12345",null,"Score");
		}
		else if(GUI.Button(new Rect(0,204,100,50),"GetCurrentScore"))
		{
			//Debug.Log("GetCurrentScore");
			OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.GET_VALUE_FOR_KEY_FROM_SERVER,"100001803572280",null,"Score");
		}
		else if(GUI.Button(new Rect(0,255,100,50),"FriendsScore"))
		{
			//Debug.Log("FriendsScore");
			OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.GET_TOP_RANKING_LIST,"",CallBack,"Score");
		}
		else if(GUI.Button(new Rect(105,255,100,50),"FriendsNScore"))
		{
			//Debug.Log("FriendsNScore");
			OnlineInterfaceHandler.Instance.SendRequest(eONLINE_REQ_TYPE.GET_TOP_N_RANKING_LIST,"5",CallBack,"Score");
		}
	}
}

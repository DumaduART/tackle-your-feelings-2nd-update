﻿using UnityEngine;
using System.Collections;

public class AnimationManual : MonoBehaviour {

	float scaleRate = 0.01f;
	float minScale  = 0.1f;
	float maxScale  = 0.5f;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		Animate ();
	}

	public void ApplyScaleRate() {

		transform.localScale += Vector3.one * scaleRate;
	}

	public void Animate() {
		//if we exceed the defined range then correct the sign of scaleRate.
		if(transform.localScale.x < minScale) {
			scaleRate = Mathf.Abs(scaleRate);
		}
		else if(transform.localScale.x > maxScale) {
			scaleRate = -Mathf.Abs(scaleRate);
		}
		ApplyScaleRate();
	}

}

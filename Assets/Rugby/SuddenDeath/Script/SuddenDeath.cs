using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class SuddenDeath : MonoBehaviour {

	// Use this for initialization
	public enum PowerType
	{
		none=0,x2,x3,x4
	};
	public PowerType powerUpType;
	public static SuddenDeath instance;
	public GameObject[] bars;
	public GameObject targetBar;
    GameObject targetClone;
	public Vector3[] targetPos;
	private List<GameObject> barscopy= new List<GameObject>();
	GameManager _gameManager;
	InputManager _inputManager;
	public int noOffball;
	bool isGoal = true;
	public int goalScore=1;
	public int noOfShotForX2;
	public int noOfShotForX3;
	public int noOfShotForX4;
	public int goalCounter;
	public int powerUpcounter;
	private int middlescore = 100;
	private int centreScore = 200;
	private int sideScore = 50;
	private GameObject ActivatedZone;
	public int x2Upgredepercentage;
	public int x3Upgredepercentage;
	public int x4Upgredepercentage;
	public int score;
	private Vector3 ballPos=new Vector3(-4,-18.5f,90);
	private int dist=0;
	private int miwindeffect;
	private int roundUpValue;
	private int targetZone =1;
	public int targetMultimlier = 1;
	public int goalStreak;
	public int currentScore;
	public int currentContinuousGoal;
	public int HighiestContinuousGoal;
	public enum WindType
	
	{
		
		right=0,
		left
	}	
	public WindType _windType;
	
	public InstructionManager instructionManager;
	
	void Awake()
	{
		instance = this;
	}
	
	void Start () {
		
		instructionManager = InstructionManager.GetInstance();
		_gameManager = GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		_inputManager = GameObject.Find("GameManager").GetComponent("InputManager") as InputManager ;
		noOffball = _gameManager.GetSuddenDeathBall();
		//GenerateZone();
		InitialzePowerUpgradePercentage();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void InitialzePowerUpgradePercentage()
	{
		x2Upgredepercentage = _gameManager.GetPowerUpUpgradePercentage(2);
		x3Upgredepercentage = _gameManager.GetPowerUpUpgradePercentage(3);
		x4Upgredepercentage	= _gameManager.GetPowerUpUpgradePercentage(4);
	}
	
	public void GenerateZone()
	{
		for(int i=0;i<bars.Length;i++)
		{
			GameObject obj = Instantiate(bars[i]) as GameObject;
			barscopy.Add(obj);
			obj.GetComponent<MeshRenderer>().enabled = false;
			EventManager.ActiveObjects.Add(obj);
		}
	}
	
	public void ActivateZone(GameObject obj)
	{
		ActivatedZone = obj;
		obj.GetComponent<MeshRenderer>().enabled = true;
	}
	
	public void DeActivateZone()
	{
		if(ActivatedZone == null)
			return ;
		ActivatedZone.GetComponent<MeshRenderer>().enabled = false;
	}
	
	public void UpdateBall()
	{
		if(isGoal)
		{
			
		}
		else
		{
			noOffball--;
		}
		
		isGoal = false;
	}
	
	public void IncreaseBall(int x)
	{
		noOffball += x;
	}
	
	public void SetGoalCondition(bool x)
	{
		isGoal = x;
	}
	
	public int GetBall()
	{
		return(noOffball);
	}
	
	public  static SuddenDeath GetInstance()
	{
		return(instance);
	}
	
	public void UpdateGoalCounter()
	{
		goalCounter++;
	}
	
	public void UpdateRoundUpParameter()
	{
		if(isGoal)
		{
			roundUpValue++;
			currentContinuousGoal++;
			if(currentContinuousGoal==3)
			{
				instructionManager.GenerateInstruction(InstructionManager.ImotionType.happy,"AMAZING, YOU SURELY KNOW HOW TO \n IMPRESS THE AUDIENCE DON’T YOU",true);
			}
			
			if(roundUpValue >= 3)
			{
				if(GetDist()<50)
				{
					dist += 5;
					miwindeffect++;
					if(SuddenDeathGUIItemsManager.instance !=null)
					{
						SuddenDeathGUIItemsManager.instance.isActivate = true;
					}
				}
				roundUpValue = 0;
				if(targetZone<=3)
					targetZone += 2;
			}
		}
		else
		{
			if(currentContinuousGoal>HighiestContinuousGoal)
				HighiestContinuousGoal = currentContinuousGoal;
			currentContinuousGoal = 0;
			roundUpValue = 0;
			if(dist>=3)
				dist -= 5;
			if(miwindeffect>=1)
				miwindeffect -=1; 
		}
		ChangeWindDirection();
	}
	
	public void UpdatePowerUpCounter()
	{
		if(isGoal)
		{
			powerUpcounter++;
			if(powerUpcounter>=noOfShotForX4)
			{
				powerUpType = PowerType.x4;
			}
			else if(powerUpcounter>=noOfShotForX3)
			{
				powerUpType = PowerType.x3;
			}
			else if(powerUpcounter>=noOfShotForX2)
			{
				powerUpType = PowerType.x2;
			}
			else 
				powerUpType = PowerType.none;
		}
		else
		{
			
			if(powerUpcounter>=noOfShotForX4)
			{
				powerUpcounter = noOfShotForX3;
				powerUpType = PowerType.x3;
			}
			else if(powerUpcounter>=noOfShotForX3)
			{
				powerUpcounter = noOfShotForX2;
				powerUpType = PowerType.x2;
			}
			else 
			{
				powerUpcounter =0;
				powerUpType = PowerType.none;
			}
				
		}
	}
	
	public void CalculateScore(string barZone)
	{
		int powerUpScore =0;
		if(barZone=="center")
		{
			switch(powerUpType)
			{
				case PowerType.x2:
				{
					powerUpScore = (int)(2*goalScore*(1.0f+x2Upgredepercentage/100.0f));
					currentScore = 2*(targetMultimlier*goalScore +powerUpScore); 
					score += 2*(targetMultimlier*goalScore +powerUpScore);   
					break;
				}
				case PowerType.x3:
				{
					powerUpScore = (int)(3*goalScore*(1.0f+x3Upgredepercentage/100.0f));
					currentScore = 2*(targetMultimlier*goalScore+powerUpScore);  
					score += 2*(targetMultimlier*goalScore+powerUpScore);      
					break;
				}
				case PowerType.x4:
				{
					powerUpScore = (int)(4*goalScore*(1.0f+x4Upgredepercentage/100.0f));
					currentScore = 2*(targetMultimlier*goalScore+powerUpScore); 
					score += 2*(targetMultimlier*goalScore+powerUpScore);   
					break;
				}
				case PowerType.none:
				{
					currentScore = 2*goalScore*targetMultimlier;
					score += 2*goalScore*targetMultimlier;
					break;
				}
					
			}
		}
		if(barZone == "gaol")
		{
			switch(powerUpType)
			{
				case PowerType.x2:
				{
					powerUpScore = (int)(2*goalScore*(1.0f+x2Upgredepercentage/100.0f));
					currentScore = (targetMultimlier*goalScore)+powerUpScore; 
					score += (targetMultimlier*goalScore)+powerUpScore;                                                                   
					break;
				}
				case PowerType.x3:
				{
					powerUpScore = (int)(3*goalScore*(1.0f+x3Upgredepercentage/100.0f));
					currentScore = (targetMultimlier*goalScore)+powerUpScore;  
					score += (targetMultimlier*goalScore)+powerUpScore;       
					break;
				}
				case PowerType.x4:
				{	
					powerUpScore = (int)(4*goalScore*(1.0f+x3Upgredepercentage/100.0f));
					currentScore = (targetMultimlier*goalScore) +powerUpScore;  
					score += (targetMultimlier*goalScore) +powerUpScore;  
					break;
				}
				case PowerType.none:
				{
					score += goalScore*targetMultimlier;
					currentScore = goalScore*targetMultimlier;
					break;
				}
					
			}                                                           
					
		}
		if(currentScore>goalStreak)
			goalStreak = currentScore;
		ScoreManager.instance.SetModeScore(score,currentScore);
	}
	
	public Vector3 ChangeBallPos()
	{
		if(_inputManager != null)
		{
			GameObject ball= _inputManager.GetRefOfBall();
			ballPos = ball.transform.position;
			ballPos.z = ballPos.z-dist;
			ballPos.x = Random.Range(-14,10);
		}
		return(ballPos);
	}
	
	public void ChangeWindDirection()
	{
		int x = Random.Range (0,2);
		switch(x)
		{
			case 0:
			{
				_windType=WindType.left;
				break;
			}
			case 1:
			{
				_windType=WindType.right;
				break;
			}
		}
		if(	SuddenDeathGUIItemsManager.instance!=null)
	   	SuddenDeathGUIItemsManager.instance.ChangeWindImage(_windType);
	}
	
	public  int GetWind()
	{
		switch(_windType)
		{
			case WindType.right:
			{
				return(miwindeffect);	
			}
			case WindType.left:
			{
				return(-miwindeffect);
			}
		   	default:
				return(0);
		}
	}
	
	public WindType GetWindType()
	{
		return(_windType);
	}
	
	public int GetScore()
	{
		return(score);
	}
	
	public int GetDist()
	{
		return((int)(115- ballPos.z));
	}
	
	public PowerType GetPowerType()
	{
		return(powerUpType);
	}
	
	public float GetTargetCloneLocalScale()
	{
		if(targetClone == null)
			return(0.0f);
		return(targetClone.transform.localScale.z);
	}
	
	public void GeneRateTarget()
	{
		int i = Random.Range(0,targetZone);
		Vector3 tempPos = targetPos[i];
		targetClone=Instantiate(targetBar,tempPos,targetBar.transform.rotation) as GameObject;
		targetClone.GetComponent<BarAnimation>().EntryAnimation();
		EventManager.ActiveObjects.Add(targetClone);
	}
	
	public void UpdateTargetMultiplier(int x)
	{
		targetMultimlier +=x;
		if(targetMultimlier<0)
			targetMultimlier=1;
	}
	
	public int GetMultiPlier()
	{
		return(targetMultimlier);
	}
	
	public int GetGoalStreak()
	{
		return(goalStreak);
	}
	
	public int GetContinuousGoal()
	{
		return(HighiestContinuousGoal);
	}
	
	public int GetCurrentcontinuousGoal()
	{
		return(currentContinuousGoal);
	}
	
	public int GetNoOfGoal()
	{
		return(goalCounter);
	}
	
	public void ChkForStatsUpdate()
	{
		StatsData statsData = _gameManager.GetStatsData();
		if(statsData._suddenDeathData._highestNoGoal<goalCounter)
		{
			statsData._suddenDeathData._highestNoGoal = goalCounter;
			_gameManager.UpdateIntDataInDevice("_suddenDeathData._highestNoGoal",statsData._suddenDeathData._highestNoGoal,false);
		}
		
		if(statsData._suddenDeathData._highestMultiplier < GetMultiPlier())
		{
			statsData._suddenDeathData._highestMultiplier = GetMultiPlier();
			_gameManager.UpdateIntDataInDevice("_suddenDeathData._highestMultiplier",statsData._suddenDeathData._highestMultiplier,false);
		}
		
		
	}
	
	public IEnumerator DestroyTarget(float target)
	{
		if(targetClone!= null)
		{
			Vector3 temp = targetClone.transform.localScale;
			while(temp.z != target)
			{
				temp.z = Mathf.MoveTowards (temp.z,target,500*Time.deltaTime);
				targetClone.transform.localScale = temp ;
				yield return new WaitForEndOfFrame() ;
				
			}
			Destroy(targetClone);
		}
	}
}

using UnityEngine;
using System.Collections;
public enum ParticleEffectType
	{
		celebration,Goal,PoleHittingLeft,waveParticle,powerUpParticle,PoleHittingRight,barHittingEffect
	}
public class ParticleEffectManager : MonoBehaviour {
	public GameObject poleLeftCollider;
	public GameObject poleRightCollider;
	
	// Use this for initialization
	public static ParticleEffectManager instance;
	private InputManager _inputManager;
	public GameObject[] celeBrationParticle;
	public GameObject goalParticle;
	public GameObject poleLeftParticle;
	public GameObject poleRightParticle;
	public GameObject waveParticle;
	public GameObject barhittingparticle;
	public GameObject[] powerUpparticle; 
	public Vector3 localPos;
	public Vector3 localScale;
	void Start () {
		instance = this;
		_inputManager = GameObject.Find("GameManager").GetComponent("InputManager") as InputManager ;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void GenerateParticle(ParticleEffectType particleType,Vector3 geneRationPoint)
	{
		switch(particleType)
		{
			case ParticleEffectType.celebration:
			{
				Instantiate(celeBrationParticle[0]);
				Instantiate(celeBrationParticle[1]);
				Instantiate(celeBrationParticle[0]);
				Instantiate(celeBrationParticle[1]);
				//Instantiate(celeBrationParticle[2]);
				break;
			}
			case ParticleEffectType.Goal:
			{
				Instantiate(goalParticle,geneRationPoint,goalParticle.transform.rotation);
				break;
			}
			case ParticleEffectType.PoleHittingLeft:
			{
//				print ("Left");
				Vector3 pos = new Vector3(geneRationPoint.x,poleLeftParticle.transform.position.y,geneRationPoint.z);
			    GameObject temp =	Instantiate(poleLeftParticle,pos,poleLeftParticle.transform.rotation) as GameObject ;
			    temp.transform.parent = poleLeftCollider.transform;
//				temp.transform.localPosition = new Vector3(-0.35f,-0.01f,0.68f); 
				break;
			}
			
			case ParticleEffectType.PoleHittingRight:
			{
//				print ("Right");
			    Vector3 pos = new Vector3(geneRationPoint.x,poleRightParticle.transform.position.y,geneRationPoint.z);
			    GameObject temp =	Instantiate(poleRightParticle,pos,poleRightParticle.transform.rotation) as GameObject ;
			    temp.transform.parent =poleRightCollider.transform;
				//temp.transform.localPosition = new Vector3(-33.0f,-0.01f,0.68f); 
				break;
			}
			case ParticleEffectType.waveParticle:
			{
				Instantiate(waveParticle,geneRationPoint,waveParticle.transform.rotation);
				break;
			}
			case ParticleEffectType.powerUpParticle:
			{
				geneRationPoint.y = -18.53f;
				switch(SuddenDeath.instance.GetPowerType())
				{
					
					case SuddenDeath.PowerType.x2:
					{
					     Instantiate(powerUpparticle[0],geneRationPoint,powerUpparticle[0].transform.rotation) ;
						break;
					}
					case SuddenDeath.PowerType.x3:
					{
						Instantiate(powerUpparticle[1],geneRationPoint,powerUpparticle[1].transform.rotation) ;
						break;
					}
					case SuddenDeath.PowerType.x4:
					{
						Instantiate(powerUpparticle[2],geneRationPoint,powerUpparticle[2].transform.rotation);
						break;
					}
				    
				}
				break;
			}
			
			case ParticleEffectType.barHittingEffect:
			{
//				geneRationPoint.z -=10;
//				geneRationPoint.y -=5;
				print ("barhitting");
			   	Vector3 temp = _inputManager.GetCameraref().transform.position;
				Vector3 dir = _inputManager.GetCameraref().transform.forward;
				temp = temp + 5*dir;
				temp.y +=2;
				Instantiate(barhittingparticle,temp,barhittingparticle.transform.rotation);
				break;
			}
		}
	}
}

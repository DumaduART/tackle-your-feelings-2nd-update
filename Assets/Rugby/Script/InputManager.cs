using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour 
{

	// Use this for initialization
	public enum InputStatus
	{
		idle=0,
		flicInput,
		rotationInput,
		capturingSwing,
		none
	};
	
	static InputManager instance;
	public InputStatus inputStatus;
	//public static InputManager instance;
	public Camera camera;
	private Vector3 mvtouchStartPos;
	private Vector3 mvtouchEndPos;
	private Vector3 mvmoveTouchStart;
	private Vector3 mvmoveTouchEnd;
//	private Vector3 mvtouchMovePos;
	public GameObject _gball;
	public int _iballDetectingRange;
	public bool mbisDetactBall;
	public GameObject Target;
//	private float mftargetDistance;
	private float mfswipeValue;
	private Vector3 mvballDirection;
	private Vector3 LateralVelocity;
	private Vector3 swingVelocity;
	private float mfStartTime;
	private Vector3 mvballThrowDir;
	public GameManager _gameManager;
	public EventManager _eventmanager;
	public int _icomfortZone;
	private   int miWindEffect = 0;
	private bool isCaptureSwing = false;
	private Rect mrDistanceSliderRect;
	private Rect mrWindSliderRect;
	public bool isSwing = true;
	public Texture tex;
	private Vector3 mballVelocity;
	private int maxAngularVelocity;
	private int t1;
	private int t2;
	public bool isRotation = true;
	public float targetSwingMag = 0;
	
	public float flickSensityvity;
	public float swingSensitivity;
	
	public int _ballSpeed;
	public int _windRsistance;
	
	void Awake()
	{
		instance = this;
	}

	void Start () 
	{
		//_gameManager=GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
	//	mftargetDistance=(Target.transform.position-_gball.transform.position).magnitude;
		//Application.targetFrameRate=400;
		inputStatus=InputStatus.idle;
		mrDistanceSliderRect=new Rect(275.0f,42.0f,18.0f,394.0f);
		mrWindSliderRect=new Rect(11.0f,42.0f,18.0f,394.0f);
		flickSensityvity = _gameManager.UpdateFloatDataInDevice("flickvalue",flickSensityvity,true);
		swingSensitivity = _gameManager.UpdateFloatDataInDevice("swingvalue",swingSensitivity,true);
		
		EventManager.Pause += Pause;
		EventManager.Resume += Resume;
		EventManager.Home += Home;
		EventManager.ReStart += ReStart;
	}

	void OnDestroy()
	{
		EventManager.Pause -= Pause;
		EventManager.Resume -= Resume;
		EventManager.Home -= Home;
		EventManager.ReStart -= ReStart;
	}
	
	public static InputManager GetInstance()
	{
		return(instance);
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{	
//		mvtouchMovePos=Input.mousePosition;
		
		if(_gameManager.startGame)
		{
			//-------------------Different Input Case------------------
			switch(inputStatus)
			{
				case InputStatus.idle://-------------Idle Case------------
					{
				        
						if(Input.GetMouseButtonDown(0)  && isRotation)
			  			{
							if(Input.mousePosition.y>Screen.height/2)
							{
								inputStatus=InputStatus.rotationInput;
							}
							if(Input.mousePosition.y<Screen.height/2 )
							{
								inputStatus=InputStatus.flicInput;
								//CheckBallDetection();
								mvtouchStartPos=Input.mousePosition;
								mfStartTime=Time.time;
				              }
					       }
						break;
					}
				case InputStatus.flicInput://------------------FlickInput case--------------------
					{
						if(Input.GetMouseButton(0))
						{
							CheckBallDetection();
						}
						mvtouchEndPos = Input.mousePosition;
						mfswipeValue = (mvtouchEndPos-mvtouchStartPos).magnitude;
						if(Input.GetMouseButtonUp(0)||mfswipeValue>Screen.height/2)
						{
							//mvtouchEndPos=Input.mousePosition;
							BallThrowDir();
					        if( (mbisDetactBall) && (((30<ChkAngel())&&(ChkAngel()<150))&&ChkDirection()>0.0f))
							{
//								 print ("finalSwipeValue"+mfswipeValue);
								_eventmanager.AddForceToball(_gball,GetVelocity(),mvballDirection);	
								//_eventmanager.AddForceToball(_gball,GetVelocity(),Quaternion.Euler(0,30,0) * (Quaternion.Euler(-45,0,0) * _gball.transform.forward.normalized));
								inputStatus=InputStatus.capturingSwing;
								mbisDetactBall=false;
					        }
					       else
								inputStatus=InputStatus.idle;
							
						}
						
						break;
			        }
				case InputStatus.rotationInput://------------------RotationInput Case------------------
					{
						if(Application.platform==RuntimePlatform.IPhonePlayer)
							_eventmanager.RotationIos(_gball);
				
						else
							_eventmanager.Rotation(_gball);
				
						if(Input.GetMouseButtonUp(0))
							inputStatus=InputStatus.idle;
						break;	
					}
				case InputStatus.capturingSwing:
					{
					    
						if(Input.GetMouseButtonDown(0))
						{
							mvmoveTouchStart=Input.mousePosition;
							isCaptureSwing = true;
							t1 = (int) Time.time;
						}
						mvmoveTouchEnd = Input.mousePosition;
				        float dx = mvmoveTouchEnd.x-mvmoveTouchStart.x;
						if((Input.GetMouseButtonUp(0)||dx>=500) &&isCaptureSwing)
						{
							//mvmoveTouchEnd=Input.mousePosition;
//							print ("dx"+dx);
							CaptureSwing();
							isCaptureSwing = false;
							t2 = (int) Time.time;
						}
						break;
					}
				
			}
			

		}
		if(_gball!=null)
		{
			float mag = _gball.GetComponent<Rigidbody>().velocity.magnitude;
			Vector3 vel = _gball.GetComponent<Rigidbody>().velocity;
			vel.x +=  miWindEffect*Time.deltaTime;
			_gball.GetComponent<Rigidbody>().velocity = mag*vel.normalized;
		}
		

		
	}
//	void FixedUpdate()
//	{
////		if(_gball!=null)
////		{
////			Vector3 vel = _gball.rigidbody.velocity;
////			swingVelocity = Vector3.Lerp(LateralVelocity/10,new Vector3(0,0,0),.01f);
////	
////			if(isSwing)
////			{
////				//float dir = _gball.rigidbody.velocity.magnitude;
////				vel.x +=  swingVelocity.x/1.0f * Time.fixedDeltaTime;
////				//vel.z +=  swingVelocity.y/2 * Time.fixedDeltaTime;
////				
////			}
////			vel.x +=  miWindEffect*Time.fixedDeltaTime;
////			//vel.z += Mathf.Abs( miWindEffect*Time.fixedDeltaTime);
////			_gball.rigidbody.velocity = vel;
////		}
//		
//		
//	}
	
	void BallThrowDir()
	{
		mfswipeValue=(mvtouchEndPos-mvtouchStartPos).magnitude;
		
		Vector3 worldPosition = camera.ScreenToWorldPoint(new Vector3(mvtouchEndPos.x,mvtouchEndPos.y,5.6f));
		 
		Vector3 ballPos = _gball.transform.position;
		
		float ycomp=Mathf.Sqrt(((worldPosition.x-ballPos.x)*(worldPosition.x-ballPos.x))+((worldPosition.z-ballPos.z)*(worldPosition.z-ballPos.z)));
		//float ycomp=Mathf.Sqrt(((mvtouchEndPos.x-mvtouchStartPos.x)*(mvtouchEndPos.x-mvtouchStartPos.x))+((mvtouchEndPos.y-mvtouchStartPos.y)*(mvtouchEndPos.y-mvtouchStartPos.y)));
		
		mvballDirection = new Vector3(worldPosition.x-ballPos.x,ycomp,worldPosition.z-ballPos.z);
		//mvballDirection = new Vector3(mvtouchEndPos.x-mvtouchStartPos.x,ycomp,mvtouchEndPos.y-mvtouchStartPos.y);
		
		mvballDirection.Normalize();
		//print (mvballDirection);
	}
	
	
	float GetVelocity()
	{
		float speed;
		float fResNormalizerVal = 1.0f;
		float fRefResVal = 960.0f;
		
		if(Screen.currentResolution.height < fRefResVal)
		{
			fResNormalizerVal = (fRefResVal / Screen.currentResolution.height);
		}

		speed=(fResNormalizerVal * mfswipeValue)/(Time.time-mfStartTime);
		
//		print (mfswipeValue);
		float velocity =(mfswipeValue/15)+speed/(2000);
		//velocity += flickSensityvity;
		if(velocity<15&&velocity>10)
			velocity=15.0f;
		
		else if(velocity<=10)
			velocity=3.0f;
		else if(velocity>=30)
			velocity=30.0f;
		return(velocity);
	}
	public int ChkAngel()
	{
	   Vector3 worldPosition = camera.ScreenToWorldPoint(new Vector3(mvtouchEndPos.x,mvtouchEndPos.y,5.6f));
		Vector3 ballPos = _gball.transform.position;
		int angel=(int)Vector3.Angle(_gball.transform.right,new Vector3(worldPosition.x-ballPos.x,0,worldPosition.z-ballPos.z));
		return(angel);
		
	}
	float ChkDirection()
	{
		Vector3 worldPosition= camera.ScreenToWorldPoint(new Vector3(mvtouchEndPos.x,mvtouchEndPos.y,70));
		Vector3 ballPos = camera.ScreenToWorldPoint(new Vector3(mvtouchStartPos.x,mvtouchStartPos.y,70));
		Vector3 dir=new Vector3(worldPosition.x-ballPos.x,0,worldPosition.y-ballPos.y);
		Vector3 crossVector=Vector3.Cross(new Vector3(dir.x,0,dir.z),Vector3.right);
		return(crossVector.y);
	}
	
	
	float SwipeSpeed()
	{
		return(mfswipeValue/(Time.time-mfStartTime));
	}
	
	
	Vector3 GetAngel()
	{
		Vector3 worldPosition= camera.ScreenToWorldPoint(mvmoveTouchEnd);
		Vector3 ballPos=_gball.transform.position;
		return(new Vector3(worldPosition.x-ballPos.x,0,worldPosition.z-ballPos.z));
	}
	
	
	void CheckBallDetection()
	{
		if(_gball==null)
			return ;
		Vector3 ballPos=Camera.main.WorldToScreenPoint(_gball.transform.position);
		float x=Input.mousePosition.x;
		float y=Input.mousePosition.y;
		if(y<ballPos.y+_iballDetectingRange && y>ballPos.y-_iballDetectingRange && x<ballPos.x+_iballDetectingRange && x>ballPos.x-_iballDetectingRange)
			mbisDetactBall=true;	
	}
	
	
	void CaptureSwing()
	{
		
		float swingVelocityMag=30.0f * ((mvmoveTouchEnd.x - mvmoveTouchStart.x) / Screen.width);
//	    LateralVelocity = new Vector3(swingVelocityMag*3,0.0f,0.0f);
//		if(isSwing)
//		{
////			_gball.rigidbody.AddForce(LateralVelocity);
//			inputStatus = InputStatus.none;
//		}
		mvmoveTouchEnd.y = mvmoveTouchStart.y;
		Vector3 worldStartPos = camera.ScreenToWorldPoint(new Vector3(mvmoveTouchStart.x,mvmoveTouchStart.y,1));
		Vector3 worldendPos = camera.ScreenToWorldPoint(new Vector3(mvmoveTouchEnd.x,mvmoveTouchEnd.y,1));
		
		if(isSwing)
			{
				
			    float temptarget = (worldendPos-worldStartPos).magnitude/7;
				temptarget *= swingSensitivity;
				if(worldendPos.x<worldStartPos.x)
				{
					temptarget = -temptarget;
				}
				targetSwingMag += temptarget;
			    if(targetSwingMag>=.4f)
					targetSwingMag = .4f;
				 if(targetSwingMag<=-.4f)
					targetSwingMag = -.4f;
//				vel.x +=  swingVelocity.x * Time.deltaTime;
//				vel.z +=  (swingVelocity.y/2 ) * Time.deltaTime;
			}
	}
	
	public float GetTargetSwing()
	{
		return(targetSwingMag);
	}
	
	public void ReSetTargetSwing()
	{
		targetSwingMag = 0.0f;
	}
	
	 public void SetInputStatus(InputStatus inputstatus)
	{
		inputStatus=inputstatus;
	}
	
	public InputStatus GetInputStatus()
	{
		return(inputStatus);
	}
	public void ResetVelocity()
	{
		mvtouchStartPos=Vector3.zero;
		mvtouchEndPos=Vector3.zero;
		swingVelocity=new Vector3(0,0,0);
		LateralVelocity=new Vector3(0,0,0);
		miWindEffect=0;
	}
	public void ResetSwingVelocity()
	{
		swingVelocity=new Vector3(0,0,0);
		LateralVelocity=new Vector3(0,0,0);
		miWindEffect=0;
	}
	
	
	public GameObject GetRefOfBall()
	{
		return(_gball);
	}
	
	
	public Camera GetCameraref()
	{
		return(camera);
	}
	

	public void SetBallRef(GameObject ballRef)
	{
		// Destroy(ballRef);
		_gball=ballRef;
	}
	public GameObject GetRefOfTarget()
	{
		return(Target);
	}
	public  void SetWindEffect(int x)
	{
		switch(_gball.GetComponent<Ball>()._ballProperty._powerType)
		{
			case PowerUpType.None:
			{
				x = x;
				break;
			}
			case PowerUpType.FireShot:
			{	
				x = x/2;
				break;
			}
			case PowerUpType.CannonBall:
			{
				x=0;
				break;
			}
				case PowerUpType.PaperSoul:
			{
				x += x/2;
				break;
			}
				case PowerUpType.Stickiee:
			{
				
				break;
			}
		}
		miWindEffect=x;
	}
	
	
	void OnGUI()
	{
		//GUI.DrawTexture(ResolutionHelper(sliderrect),tex);
		//GUI.DrawTexture(ResolutionHelper(sliderrect),tex);
	}
	
	
	
	public Rect ResolutionHelper(Rect rect)
    {
        float widthfactor=Screen.width/320.0f;
        float heightfactor=Screen.height/480.0f;
        
        return new Rect(rect.x*widthfactor,rect.y*heightfactor,rect.width*widthfactor,rect.height*heightfactor);
    }
    
    public bool CheckInsideRect(Rect rectToCheck)
    {
       rectToCheck.y=Screen.height-rectToCheck.y-rectToCheck.height; 
	   return rectToCheck.Contains(Input.mousePosition);
    }
	public void ChangeSwingCondition()
	{
		isSwing=false;
	}
	
	private void Pause()
	{
		if(_gball==null)
			return ;
		mballVelocity = _gball.GetComponent<Rigidbody>().velocity;
		maxAngularVelocity = (int)_gball.GetComponent<Rigidbody>().maxAngularVelocity;
		_gball.transform.GetComponent<Rigidbody>().useGravity=false;
		_gball.transform.GetComponent<Rigidbody>().isKinematic=true;
		camera.GetComponent<CameraMovement>().enabled=false;
		this.enabled=false;
		if(inputStatus == InputStatus.idle || inputStatus == InputStatus.flicInput)
		{
			inputStatus = InputStatus.idle;
		}
	}
	public void SetSwingCondition(bool x)
	{
		if(x)
			isSwing=false;
		else
			isSwing=true;
	}
	private void Resume()
	{
		if(_gball==null)
			return ;
		this.enabled=true;
		 if(inputStatus!=InputManager.InputStatus.idle)
		{
			_gball.transform.GetComponent<Rigidbody>().useGravity=true;
		}
		 _gball.transform.GetComponent<Rigidbody>().isKinematic=false;
		_gball.GetComponent<Rigidbody>().velocity=mballVelocity;
		_gball.GetComponent<Rigidbody>().maxAngularVelocity=maxAngularVelocity;
		camera.GetComponent<CameraMovement>().enabled=true;
	}
	
	private void Home()
	{
		camera.transform.position=new Vector3 (-3.94f,-18.0975f,93.608f);
		camera.transform.rotation= new Quaternion(0,0,0,0);
		camera.transform.eulerAngles=new Vector3(354,0,0);
	}
	private void ReStart()
	{
//		this.enabled=true;
		//camera.transform.position=new Vector3 (-3.94f,-18.0975f,93.608f);
//		camera.transform.rotation= new Quaternion(0,0,0,0);
//		camera.transform.eulerAngles=new Vector3(354,0,0);
	}
	
	public void SetRotationCondition(bool x)
	{
		isRotation = x;
	}
	
	public bool GetRotationCondition()
	{
		return(isRotation);
	}
	
	public void Setsensityvity(float x,float y)
	{
		if(x>0)
		flickSensityvity = 10*x;
		//swingSensitivity = 2+y;
		_gameManager.UpdateFloatDataInDevice("flickvalue",flickSensityvity,false);
		_gameManager.UpdateFloatDataInDevice("swingvalue",swingSensitivity,false);
	}
	
	public void SetBallParameter(float ballSpeed,float windResistance,float swingAngel)
	{
		_ballSpeed = (int)ballSpeed;
		_windRsistance = (int)windResistance;
		swingSensitivity = swingAngel;
	}
}
 

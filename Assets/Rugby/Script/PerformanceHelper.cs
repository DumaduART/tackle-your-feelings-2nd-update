﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PerformanceDataMaterial
{
	public GameObject _OriginalObj;
	public Material _AlternateMaterial;
	public Vector2 _TileOffset = new Vector2(0,0);
	public Vector2 _TilingValue = new Vector2(1,1);
}

[System.Serializable]
public class PerformanceDataPrefab
{
	public GameObject _OriginalObj;
	public GameObject _NewObjectPrefab;

	public Vector3 _Scale = new Vector3(1,1,1);
	public Vector3 _Position = new Vector3(0,0,0);
	public Vector3 _Rotation = new Vector3(0,0,0);
	public bool _ModifiedTransformValues = false;
}

public class PerformanceHelper : MonoBehaviour 
{
	public PerformanceDataMaterial[] _MaterialOptimizeArray;
	public PerformanceDataPrefab[] _ObjectOptimizeArray;
	public Vector2 UseForRatio = new Vector2(9,16);	
	// Use this for initialization
	void Start () 
	{
		float tRatio = (float)Screen.width/(float)Screen.height;
		float tRequiredRatio = UseForRatio.x/UseForRatio.y;
		float diff = Mathf.Abs(tRequiredRatio - tRatio);
		if(diff <= 0.1f)
		{
			for(int i = 0; i < _MaterialOptimizeArray.Length;i++)
			{
				GameObject tObj = _MaterialOptimizeArray[i]._OriginalObj;
				tObj.GetComponent<Renderer>().material = _MaterialOptimizeArray[i]._AlternateMaterial;
				tObj.GetComponent<Renderer>().material.mainTextureOffset = _MaterialOptimizeArray[i]._TileOffset;
				tObj.GetComponent<Renderer>().material.mainTextureScale = _MaterialOptimizeArray[i]._TilingValue;
			}
			
			for(int i = 0; i < _ObjectOptimizeArray.Length;i++)
			{
				GameObject tObj = _ObjectOptimizeArray[i]._OriginalObj;
				string tName = tObj.name;
				int tLayer = tObj.layer;
				Transform tParent = null;
				if(tObj.transform.parent)
					tParent = tObj.transform.parent;
				
				GameObject tNewObject= Instantiate(_ObjectOptimizeArray[i]._NewObjectPrefab) as GameObject;
				if(_ObjectOptimizeArray[i]._ModifiedTransformValues)
				{
					if(tParent != null)
						tNewObject.transform.parent = tParent;
					
					tNewObject.transform.localPosition = _ObjectOptimizeArray[i]._Position;
					tNewObject.transform.localScale = _ObjectOptimizeArray[i]._Scale;
					tNewObject.transform.localEulerAngles = _ObjectOptimizeArray[i]._Rotation;
				}
				else
				{
					if(tParent != null)
						tNewObject.transform.parent = tParent;
					
					tObj.transform.localPosition = tNewObject.transform.localPosition;
					tObj.transform.localScale = tNewObject.transform.localScale;
					tObj.transform.localEulerAngles = tNewObject.transform.localEulerAngles;
				}
				tNewObject.name = tName;
				tNewObject.layer = tLayer;
				Destroy(tObj);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

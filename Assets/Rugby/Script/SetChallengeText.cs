﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SetChallengeText : MonoBehaviour {
	
    public Text _instructionGUIText;
	// Use this for initialization
	void Start () {
	
	}
	
	public void SetText(string msInstruction)
	{
		_instructionGUIText.text = msInstruction;
		
	}
}

﻿using UnityEngine;
using System.Collections;

public class QuqlityHandler : MonoBehaviour {

	// Use this for initialization
	public GameObject[] _gActivateObject;
	
	void Start () {
		ChangeQuality();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	public void ChangeQuality()
	{
		#if UNITY_IPHONE
		if(UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPad3Gen || UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone5 || UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouch5Gen)
		{
			for(int i = 0;i<_gActivateObject.Length;i++)
			{
				_gActivateObject[i].SetActive(true);
			}
		}
		
		else
		{
			for(int i = 0;i<_gActivateObject.Length;i++)
			{
				_gActivateObject[i].SetActive(false);
			}
		}
		#elif UNITY_ANDROID
			float screenSize;
			for(int i = 0;i<_gActivateObject.Length;i++)
			{
				 
				if(Screen.height>Screen.width)
					screenSize = Screen.width;
				else
					screenSize = Screen.height;
				if(screenSize>320)
					_gActivateObject[i].SetActive(true);
				else
					_gActivateObject[i].SetActive(false);
			}
		#endif
			
	}
}

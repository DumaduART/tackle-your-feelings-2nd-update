// all event sound handling

using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public static SoundManager instance = null;
	public AudioClip _normalButtonSound,_guiSound,_normalGamePlaySound,ballThrowingSond,_onMissSound,_goalPostHitSound;
	public AudioClip _throughRingsSound;
	public GameManager _gameManager;
	public AudioClip[] collisionSound,inputSound ,_onGoalSound,_whistleSound;
	
	public float sound =1;
	public float sfxSound = 1;
	public AudioSource _bgAudio;
	public AudioSource _eventAudio;
	// game play sound
	
	public enum soundstateInGame
	{
		gui,gameplay
	};
	public soundstateInGame soundState;
	
	
	
	void Awake()
	{
		instance = this;
	}
	
	void Start()
	{
		_gameManager = GameObject.Find("GameManager").GetComponent("GameManager") as GameManager ;
		//sfxSound = _gameManager.UpdateFloatDataInDevice("sfxvalue",1,true) - 0.493f;
		//sound = _gameManager.UpdateFloatDataInDevice("musicvalue",1,true) - 0.493f;
		SetSond();
	}
	
	public void SetSond()
	{
		sfxSound = _gameManager.UpdateFloatDataInDevice("sfxvalue",1,true) - 0.493f;
		sound = _gameManager.UpdateFloatDataInDevice("musicvalue",1,true) - 0.493f;
		transform.gameObject.GetComponent<AudioSource>().volume = sound;

	}
	
	public void SoundStatefunction(soundstateInGame soundStatevar)
	{
		sound = PlayerPrefs.GetInt("sound");
		if(PlayerPrefs.GetString("sound") == "on" )
		{
			if(soundStatevar == soundstateInGame.gui)
			{
					_bgAudio.GetComponent<AudioSource>().Stop();
					_bgAudio.GetComponent<AudioSource>().clip = _guiSound;
					_bgAudio.GetComponent<AudioSource>().Play();
				
			}
			
			else if(soundStatevar == soundstateInGame.gameplay)
			{
					_bgAudio.GetComponent<AudioSource>().Stop();
					_bgAudio.GetComponent<AudioSource>().clip = _normalGamePlaySound;
					_bgAudio.GetComponent<AudioSource>().Play();
			
				
			}
		}
		else if(PlayerPrefs.GetString("sound") == "off" )
		{
			if(soundStatevar == soundstateInGame.gui)
			{
	
					_bgAudio.GetComponent<AudioSource>().clip = _guiSound;
					_bgAudio.GetComponent<AudioSource>().Stop();
			}
			else if(soundStatevar == soundstateInGame.gameplay)
			{

					_bgAudio.GetComponent<AudioSource>().clip = _normalGamePlaySound;
					_bgAudio.GetComponent<AudioSource>().Stop();
			}
		}
	
	}
	
	public  void NormalButtonSoundEvent()
	{
		if(_normalButtonSound != null && PlayerPrefs.GetString("sound") == "on" )
			//AudioSource.PlayClipAtPoint(_normalButtonSound,new Vector3(0,0, -18),sfxSound);	
			AudioSource.PlayClipAtPoint(_normalButtonSound,this.transform.position,sfxSound);	

	}
	
	public void BallThrowingEvent()
	{
		if(ballThrowingSond != null && PlayerPrefs.GetString("sound") == "on" )
			AudioSource.PlayClipAtPoint(ballThrowingSond,new Vector3(0,0, -18),sfxSound);	
	}
	
	public void inputEvent(int i)
	{
			if(inputSound != null && PlayerPrefs.GetString("sound") == "on")
				AudioSource.PlayClipAtPoint(inputSound[i],Vector3.zero,sfxSound);
		
		
	}
	
	public void WindSoundeffect()
	{
//		if(_windEffectsound != null && PlayerPrefs.GetString("sound")=="on")
//		{
//			AudioSource.PlayClipAtPoint(_windEffectsound,Vector3.zero,sfxSound);
//		}
	}
	
	public void WhistleSound(int i)
	{
		if(_whistleSound[i] != null && PlayerPrefs.GetString("sound")=="on")
		{
			AudioSource.PlayClipAtPoint(_whistleSound[i],Vector3.zero,sfxSound);
		}
	}
	
	public void OnGoal(int i)
	{
		if(_onGoalSound[i] != null && PlayerPrefs.GetString("sound")=="on")
		{
			AudioSource.PlayClipAtPoint(_onGoalSound[i],Vector3.zero,sfxSound);
		}
	}
	
	public void OnMissed()
	{
		if(_onMissSound != null && PlayerPrefs.GetString("sound")=="on")
		{
			AudioSource.PlayClipAtPoint(_onMissSound,Vector3.zero,sfxSound);
		}
	}
	
	public void GoalPost()
	{
		if(_goalPostHitSound != null && PlayerPrefs.GetString("sound")=="on")
		{
			AudioSource.PlayClipAtPoint(_goalPostHitSound,Vector3.zero,sfxSound);
		}
	}
	
	public void XpBarfillingSound()
	{
//		if(_xpBarFillingSound != null && PlayerPrefs.GetString("sound")=="on")
//		{
//			AudioSource.PlayClipAtPoint(_xpBarFillingSound,Vector3.zero,sfxSound);
//		}
	}
	
	public void LevelUpSound()
	{
//		if(_levelUpSound != null && PlayerPrefs.GetString("sound")=="on")
//		{
//			AudioSource.PlayClipAtPoint(_levelUpSound,Vector3.zero,sfxSound);
//		}
	}
	
	public void PassThroughRingsSound()
	{
		
		if(_throughRingsSound != null && PlayerPrefs.GetString("sound")=="on")
		{
//			print ("throughringsSound");
			AudioSource.PlayClipAtPoint(_throughRingsSound,Vector3.zero,sfxSound);
		}
	}
	
	
	
}
